package ee.ut.jf2014.homework1;

import org.apache.commons.lang.ArrayUtils;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.Path;

public class FileByteReverser {

    private static final int BUF_SIZE = 8192;

    public static void reverse(Path path) throws IOException {

        RandomAccessFile file = new RandomAccessFile(path.toFile(), "rw");

        byte[] bufBeg = new byte[BUF_SIZE];
        byte[] bufEnd = new byte[BUF_SIZE];

        long offsetBeg, offsetEnd;

        try {

            long fLength = file.length();

            offsetBeg = 0;
            offsetEnd = fLength - BUF_SIZE;

            // While buffers do not overlap, switch and reverse complete segments of size BUF_SIZE.
            while (offsetBeg + BUF_SIZE <= offsetEnd) {

                // Read segment in the first half of the file.
                file.seek(offsetBeg);
                file.read(bufBeg, 0, BUF_SIZE);

                // Read segment in the last half of the file.
                file.seek(offsetEnd);
                file.read(bufEnd, 0, BUF_SIZE);

                // Reverse segments.
                ArrayUtils.reverse(bufBeg);
                ArrayUtils.reverse(bufEnd);

                // Write reversed segments instead of segments from the other halves.
                file.seek(offsetBeg);
                file.write(bufEnd, 0, BUF_SIZE);
                file.seek(offsetEnd);
                file.write(bufBeg, 0, BUF_SIZE);

                // Move closer to the center.
                offsetBeg += BUF_SIZE;
                offsetEnd -= BUF_SIZE;
            }

            // Calculate size of remaining non-overlapping segments.
            int remLen = (int) (fLength / 2 - offsetBeg);

            // Move offset of second segment to appropriate position.
            offsetEnd += (BUF_SIZE - remLen);

            // Read segments. Align them to the ends of the buffers.
            file.seek(offsetBeg);
            file.read(bufBeg, BUF_SIZE - remLen, remLen);
            file.seek(offsetEnd);
            file.read(bufEnd, BUF_SIZE - remLen, remLen);

            // Reverse segments.
            ArrayUtils.reverse(bufBeg);
            ArrayUtils.reverse(bufEnd);

            // Write reversed segments instead of segments from the other halves.
            file.seek(offsetBeg);
            file.write(bufEnd, 0, remLen);
            file.seek(offsetEnd);
            file.write(bufBeg, 0, remLen);

        } finally {
            file.close();
        }
    }
}
