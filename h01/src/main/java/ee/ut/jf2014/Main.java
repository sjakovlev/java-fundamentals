package ee.ut.jf2014;

import ee.ut.jf2014.homework1.FileByteReverser;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Main {
    public static void main(String[] args) {

        if (args.length < 1) {
            System.out.println("Usage: program path/to/file.txt");
            return;
        }

        Path targetPath = Paths.get(args[0]);
        if (!Files.exists(targetPath)) {
            System.out.println("Error: file does not exist.");
            return;
        }

        if (!Files.isRegularFile(targetPath)) {
            System.out.println("Error: not a file.");
            return;
        }

        if (!Files.isReadable(targetPath) || !Files.isWritable(targetPath)) {
            System.out.println("Error: file not readable or not writable.");
            return;
        }

        System.out.println(String.format("File name: %s", targetPath.getFileName()));

        try {
            double targetSizeInKB = Files.size(targetPath) / 1000;
            System.out.println(String.format("Size: %.0f kB", targetSizeInKB));

            long timeStart = System.currentTimeMillis();
            FileByteReverser.reverse(targetPath);
            long timeEnd = System.currentTimeMillis();

            double timeInSec = ((double)(timeEnd - timeStart) / 1000.0);

            System.out.println(String.format("Duration: %.2f s", timeInSec));
            System.out.println(String.format("Speed: %.0f kB/s", targetSizeInKB / timeInSec));

        } catch (IOException e) {
            System.out.println("I/O error.");
        }
    }
}
