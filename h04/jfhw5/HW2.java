/*
 * Copyright (c) 2014, 2014, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 */
package org.openjdk.jcstress.tests.jfhw5;

import org.openjdk.jcstress.annotations.Actor;
import org.openjdk.jcstress.annotations.Description;
import org.openjdk.jcstress.annotations.Expect;
import org.openjdk.jcstress.annotations.JCStressTest;
import org.openjdk.jcstress.annotations.Outcome;
import org.openjdk.jcstress.annotations.State;
import org.openjdk.jcstress.infra.results.IntResult3;
import sun.misc.Contended;

/**
 * Result [0, 0, 0] is possible.
 * 
 * Since all used fields are non-volatile, there are no synchronization actions
 * between these threads.
 * 
 * It is easy to see that we get this result if, for example, threads 3 and 4
 * complete before the last operation in thread 2, and thread 2 first operation
 * completes before thread 1 starts. This means that there is an allowed
 * operation ordering that generates this result. Consequently, this result is
 * possible.
 * 
 * @author Sergei Jakovlev
 *
 */
@JCStressTest
@Description("")
@Outcome(id = { "[0, 0, 0]", "[0, 1, 0]", "[0, 1, 1]", "[1, 0, 0]" }, expect = Expect.ACCEPTABLE, desc = "Acceptable under sequential consistency")
@Outcome(id = { "[0, 0, 1]", "[1, 0, 1]", "[1, 1, 0]", "[1, 1, 1]" }, expect = Expect.FORBIDDEN, desc = "Frobidden under sequential consistency")
@State
public class HW2 {

	@Contended
	int x = 0;

	@Contended
	int y = 0;

	@Contended
	int z = 0;

	@Actor
	public void actor1() {
		z = 1;
	}

	@Actor
	public void actor2(IntResult3 r) {
		r.r1 = z;
		if (r.r1 == 0) {
			x = 1;
		}
	}

	@Actor
	public void actor3(IntResult3 r) {
		r.r2 = x;
		y = r.r2;
	}

	@Actor
	public void actor4(IntResult3 r) {
		r.r3 = y;
		x = r.r3;
	}

}
