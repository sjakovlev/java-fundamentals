/*
 * Copyright (c) 2014, 2014, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 */
package org.openjdk.jcstress.tests.jfhw5;

import org.openjdk.jcstress.annotations.Actor;
import org.openjdk.jcstress.annotations.Description;
import org.openjdk.jcstress.annotations.Expect;
import org.openjdk.jcstress.annotations.JCStressTest;
import org.openjdk.jcstress.annotations.Outcome;
import org.openjdk.jcstress.annotations.State;
import org.openjdk.jcstress.infra.results.IntResult4;

import sun.misc.Contended;

/**
 * Result [1, 0, 1, 0] is impossible considering sequential consistency.
 * 
 * Since v2 and v2 are volatile, writes in threads 1 and 2 and reads in 3 and 4
 * are synchronization actions. That means that compiler cannot reorder
 * operations in threads 3 and 4 since that would violate Happens-Before
 * ordering (we would be bringing r2 and r4 writes out of HB ordering).
 * 
 * In order to get [1, 0, 1, 0], following conditions would have to be
 * satisfied:
 * 
 * 1. Operation in thread 1 would have to complete before first operation in
 * thread 3.
 * 
 * 2. Operation in thread 2 can run only after second operation in thread 3 is
 * completed.
 * 
 * 3. Operation in thread 2 would have to complete before first operation in
 * thread 4.
 * 
 * 4. Operation in thread 1 can run only after second operation in thread 4 is
 * completed.
 * 
 * We cannot satisfy all 4 conditions without reordering operations in thread 3
 * and 4. Consequently, this result is impossible.
 * 
 * As a side note, getting [0, 1, 0, 1] is possible, but "window of opportunity"
 * in this case is quite small (that is why I consider this result
 * "interesting").
 * 
 * @author Sergei Jakovlev
 *
 */
@JCStressTest
@Description("")
@Outcome(id = { "[0, 0, 0, 0]", "[0, 0, 0, 1]", "[0, 0, 1, 0]", "[0, 0, 1, 1]",
		"[0, 1, 0, 0]", "[0, 1, 1, 0]", "[0, 1, 1, 1]", "[1, 0, 0, 0]",
		"[1, 0, 0, 1]", "[1, 0, 1, 1]", "[1, 1, 0, 0]", "[1, 1, 0, 1]",
		"[1, 1, 1, 0]", "[1, 1, 1, 1]" }, expect = Expect.ACCEPTABLE, desc = "Acceptable under sequential consistency")
@Outcome(id = { "[0, 1, 0, 1]" }, expect = Expect.ACCEPTABLE_INTERESTING, desc = "Acceptable under sequential consistency")
@Outcome(id = { "[1, 0, 1, 0]" }, expect = Expect.FORBIDDEN, desc = "Frobidden under sequential consistency")
@State
public class HW1 {

	@Contended
	volatile int v1 = 0;

	@Contended
	volatile int v2 = 0;

	@Actor
	public void actor1() {
		v1 = 1;
	}

	@Actor
	public void actor2() {
		v2 = 1;
	}

	@Actor
	public void actor3(IntResult4 r) {
		r.r1 = v1;
		r.r2 = v2;
	}

	@Actor
	public void actor4(IntResult4 r) {
		r.r3 = v2;
		r.r4 = v1;
	}

}
