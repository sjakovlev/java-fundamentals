package org.zeroturnaround.jf.hw.remote;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.util.Arrays;

public class RemotePluginLoader extends ClassLoader {

	/**
	 * PNG is read in blocks of this size
	 */
	private final static int BLOCK_SIZE = 4096;

	/**
	 * Java class marker
	 */
	private final static byte MARKER[] = { (byte) 0xCA, (byte) 0xFE, (byte) 0xBA, (byte) 0xBE };

	/**
	 * PNG file marker
	 */
	private final static byte PNG_MARKER[] = { (byte) 137, (byte) 80, (byte) 78, (byte) 71, (byte) 13, (byte) 10,
			(byte) 26, (byte) 10 };

	private final URL pluginLocation;

	public RemotePluginLoader(String pluginName, URL pluginLocation) {
		this.pluginLocation = pluginLocation;
	}

	@Override
	public Class<?> loadClass(String name) throws ClassNotFoundException {
		Class<?> clazz;

		// Try to load using parent
		try {
			clazz = getParent().loadClass(name);
			return clazz;
		} catch (ClassNotFoundException e) {
		}

		// Create absolute URL to PNG file containing class.
		String classPngFileLocation = name.substring(name.lastIndexOf(".") + 1) + ".png";
		URL classPngCompletePath;
		try {
			classPngCompletePath = new URL(pluginLocation, classPngFileLocation);
		} catch (MalformedURLException e1) {
			throw new RuntimeException(e1);
		}

		// Process PNG file
		try (BufferedInputStream inputStream = new BufferedInputStream(classPngCompletePath.openStream())) {
			// Read image in blocks of size BLOCK_SIZE
			ByteArrayOutputStream image = new ByteArrayOutputStream();
			byte buf[] = new byte[BLOCK_SIZE];
			int read;
			while ((read = inputStream.read(buf)) != -1) {
				image.write(buf, 0, read);
			}

			byte imageBytes[] = image.toByteArray();

			// Find class location within image (if any).
			int pos = findMarker(imageBytes);

			if (pos == -1) {
				// Class not found in image
				System.out.println("Unable to find class " + name);
				throw new ClassNotFoundException(name);
			}

			return defineClass(name, imageBytes, pos, imageBytes.length - pos);

		} catch (IOException e) {
			throw new RuntimeException(e);
		}

	}

	/**
	 * Find position of Java class within PNG file represented by byte array.
	 * 
	 * Note that this method considers PNG structure. Class will only be found
	 * if it is located immediately after the end of PNG chunk.
	 * 
	 * @param data
	 *            byte array containing valid PNG file
	 * @return offset of class in given array or -1 if class was not found.
	 */
	private static int findMarker(byte[] data) {

		// Check if actually got PNG.
		byte pngMarker[] = Arrays.copyOf(data, 8);
		if (!Arrays.equals(pngMarker, PNG_MARKER)) {
			// Not a PNG
			return -1;
		}

		// Check possible chunk locations in search of a marker.
		// Note that this method assumes that marker is located immediately
		// after correct PNG chunk.
		int pos = 8;
		long chunkLength = 0;
		try {
			while (pos < data.length) {

				// Decode length of next chunk
				chunkLength = ByteBuffer.wrap(data, pos, 4).getInt() & 0xffffffffL;

				// Calculate new offset
				pos += 4 + 4 + chunkLength + 4;

				// Check if class marker is located there
				int j = 0;
				while (data[pos + j] == MARKER[j]) {
					j++;
					if (j >= MARKER.length) {
						return pos;
					}
				}
			}
		} catch (IndexOutOfBoundsException e) {
			// Reading garbage
			return -1;
		}
		// Marker not found
		return -1;
	}
}
