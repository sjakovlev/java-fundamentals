package org.zeroturnaround.jf.hw.remote;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.zeroturnaround.jf.hw.Plugin;

public class RemotePluginManager {
	
	private final static String pluginRoot = "https://raw.github.com/zeroturnaround/jf-hw-classloaders/master/plugins-remote";

	public static String[] findAllPlugins() {
		return findAllPluginInfos().keySet().toArray(new String[] {});
	}

	private static Map<String, String> findAllPluginInfos() {
		return new HashMap<String, String>() {
			private static final long serialVersionUID = 1L;

			{
				put("NomNomNomPlugin",
						"https://raw.github.com/zeroturnaround/jf-hw-classloaders/master/plugins-remote/NomNomNomPlugin/README.properties");
				put("ChickenPlugin",
						"https://raw.github.com/zeroturnaround/jf-hw-classloaders/master/plugins-remote/ChickenPlugin/README.properties");
				put("HeadAndShouldersPlugin",
						"https://raw.github.com/zeroturnaround/jf-hw-classloaders/master/plugins-remote/HeadAndShouldersPlugin/README.properties");
			}
		};
	}

	public static Plugin getPluginInstance(String pluginName) {
		Map<String, String> pluginInfos = findAllPluginInfos();

		URL pluginPropertiesUrl;

		try {
			pluginPropertiesUrl = new URL(pluginInfos.get(pluginName));
		} catch (MalformedURLException e) {
			// Trying to load unknown plugin or malformed plugin URL.
			throw new RuntimeException(e);
		}

		try (BufferedInputStream inputStream = new BufferedInputStream(pluginPropertiesUrl.openStream())){
			Properties properties = new Properties();
			try {
				properties.load(inputStream);
			} catch (IOException e) {
				throw new RuntimeException(e);
			}

			String pluginClass = (String) properties.getProperty("class");

			ClassLoader cl = new RemotePluginLoader(pluginName, new URL(pluginRoot + "/" + pluginName + "/"));
			Class<?> clazz = null;

			try {
				//
				clazz = cl.loadClass(pluginClass);
			} catch (ClassNotFoundException e) {
				throw new RuntimeException(e);
			}

			try {
				return (Plugin) clazz.newInstance();
			} catch (InstantiationException e) {
				throw new RuntimeException(e);
			} catch (IllegalAccessException e) {
				throw new RuntimeException(e);
			}
		} catch (IOException e) {
			// Failed to open properties file or malformed URL
			throw new RuntimeException(e);
		}

		
	}
}
