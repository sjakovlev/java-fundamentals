package ee.ut.jf2014.homework12;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtConstructor;
import javassist.CtField;
import javassist.CtMethod;
import javassist.CtNewConstructor;
import javassist.CtNewMethod;
import javassist.CtPrimitiveType;
import javassist.NotFoundException;

public final class CustomProxyFactory_OLD {

	/**
	 * Return a new dynamic proxy implementing specified interface and using
	 * provided object.
	 * 
	 * Returned proxy will behave in the following way:
	 * 
	 * 1. If a method exists on obj that meets all the requirements for it to
	 * implement the invoked interface method, invoke that method on obj and
	 * return the result, unless the method is marked as deprecated.
	 * 
	 * 2. If the invoked method adhere to the JavaBean conventions regarding
	 * property methods (getters/setters), and such a field exists, access the
	 * field, either getting or setting its value.
	 * 
	 * 3. If no match for (1) or (2) was found, throw a NoSuchMethodException.
	 * 
	 * @param iface
	 *            interface for the proxy class to implement
	 * @param obj
	 *            object on which the methods will be invoked
	 * @return a proxy instance
	 * @throws NotFoundException
	 */

	public static final Object proxify(Class<?> iface, Object obj) {
		try {
			return proxify_real(iface, obj);
		} catch (NotFoundException e) {
			throw new RuntimeException(e);
		} catch (CannotCompileException e) {
			throw new RuntimeException(e);
		}
	}

	private static final Object proxify_real(Class<?> iface, Object obj) throws NotFoundException,
			CannotCompileException {

		ClassPool cp = new ClassPool(true);

		cp.importPackage("java.lang.reflect.Field");

		cp.importPackage("ee.ut.jf2014.homework12.ProxyInvocationHandler");

		CtClass clazz = cp.makeClass("CustomDynamicProxy");

		CtClass ctInterface = cp.get(iface.getName());
		clazz.addInterface(ctInterface);

		CtClass ctDelegate = cp.get(obj.getClass().getName());

		// TODO
		CtClass ctHandler = cp.get(ProxyInvocationHandler.class.getName());
		CtField handler = new CtField(ctHandler, "handler", clazz);
		clazz.addField(handler);
		// TODO

		// Create delegate field and appropriate constructor
		CtField delegate = new CtField(ctDelegate, "delegate", clazz);
		delegate.setModifiers(Modifier.PRIVATE);
		clazz.addField(delegate);
		CtConstructor constructor = CtNewConstructor.make(new CtClass[] { ctDelegate }, new CtClass[] {}, ""
				+ "{delegate = $1;" + " handler = new " + ProxyInvocationHandler.class.getName() + "($1);}", clazz);
		clazz.addConstructor(constructor);

		// Get all interface methods.

		List<CtMethod> interfaceMethods = new LinkedList<CtMethod>();

		CtClass curInterface = ctInterface;

		while (curInterface != null && curInterface.isInterface()) {
			CtMethod[] curInterfaceMethods = curInterface.getDeclaredMethods();

			for (CtMethod curInterfaceMethod : curInterfaceMethods) {
				interfaceMethods.add(curInterfaceMethod);
			}

			curInterface = curInterface.getSuperclass();
		}

		// Get all delegate fields

		HashMap<String, CtField> fields = processClassFields(ctDelegate);

		for (CtMethod method : interfaceMethods) {
			

			if (method.getReturnType().equals(CtPrimitiveType.voidType)) {
				CtMethod proxyMethod = CtNewMethod.make(method.getReturnType(), method.getName(),
						method.getParameterTypes(), method.getExceptionTypes(), "{handler.invoke($0, this.getClass().getDeclaredMethod(\"" + method.getName() + "\", $sig), $args);}", clazz);
				clazz.addMethod(proxyMethod);
			} else {
				CtMethod proxyMethod = CtNewMethod.make(method.getReturnType(), method.getName(),
						method.getParameterTypes(), method.getExceptionTypes(), "{return ($r) handler.invoke($0, this.getClass().getDeclaredMethod(\"" + method.getName() + "\", $sig), $args);}", clazz);
				clazz.addMethod(proxyMethod);
			}

			// try {
			// addDelegatedMethod(clazz, method, ctDelegate);
			// } catch (NotFoundException e) {
			//
			// try {
			// addBeansGetter(clazz, method, fields);
			// } catch (NotFoundException e2) {
			// addBeansSetter(clazz, method, fields);
			// }
			//
			// } catch (SecurityException | CannotCompileException e) {
			// throw new RuntimeException(e);
			// }
		}

		try {
			return clazz.toClass().getDeclaredConstructor(obj.getClass()).newInstance(obj);
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException | CannotCompileException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	private static String addBeansGetter(CtClass clazz, CtMethod method, HashMap<String, CtField> fields)
			throws NotFoundException, CannotCompileException {
		String methodName = method.getName();
		String fieldName;

		if (method.getParameterTypes().length != 0) {
			throw new NotFoundException("Wrong parameter count for getter " + methodName);
		}

		if (methodName.startsWith("get") && methodName.length() > 3) {
			fieldName = getBeansFieldName(methodName, 3);
		} else if (methodName.startsWith("is") && methodName.length() > 2
				&& method.getReturnType().equals(boolean.class)) {
			// Boolean getter
			fieldName = getBeansFieldName(methodName, 2);
		} else {
			throw new NotFoundException("Wrong method name for getter " + methodName);
		}

		CtField field = fields.get(fieldName);

		if (field == null) {
			throw new NotFoundException("Required field \"" + fieldName + "\" not found");
		}

		if (!field.getType().equals(method.getReturnType())) {
			throw new NotFoundException("Field type mismatch");

		}

		CtMethod proxyMethod = CtNewMethod.make(method.getReturnType(), methodName, method.getParameterTypes(),
				method.getExceptionTypes(), "" + "{try {" + "		Field f = delegate.getClass().getDeclaredField(\""
						+ fieldName + "\");" + "		f.setAccessible(true);" + "		return ($r) f.get(delegate);"
						+ "} catch (Throwable e1) {" + "		throw new RuntimeException(e1);" + "}}", clazz);
		clazz.addMethod(proxyMethod);
		return fieldName;
	}

	private static String addBeansSetter(CtClass clazz, CtMethod method, HashMap<String, CtField> fields)
			throws NotFoundException, CannotCompileException {
		String methodName = method.getName();
		String fieldName;

		if (method.getParameterTypes().length != 1) {
			throw new NotFoundException("Wrong parameter count for setter " + methodName);
		}

		if (methodName.startsWith("set") && methodName.length() > 3) {
			fieldName = getBeansFieldName(methodName, 3);
		} else {
			throw new NotFoundException("Wrong method name for setter " + methodName);
		}

		CtField field = fields.get(fieldName);

		if (field == null) {
			throw new NotFoundException("Required field \"" + fieldName + "\" not found");
		}

		if (!field.getType().equals(method.getParameterTypes()[0])) {
			throw new NotFoundException("Field type mismatch for setter " + methodName);

		}

		if (!CtPrimitiveType.voidType.equals(method.getReturnType())) {
			throw new NotFoundException("Return type mismatch for setter " + methodName);

		}

		CtMethod proxyMethod = CtNewMethod.make(method.getReturnType(), methodName, method.getParameterTypes(),
				method.getExceptionTypes(), "" + "{try {" + "		Field f = delegate.getClass().getDeclaredField(\""
						+ fieldName + "\");" + "		f.setAccessible(true);" + "		f.set(delegate, $1);"
						+ "} catch (Throwable e1) {" + "		throw new RuntimeException(e1);" + "}}", clazz);
		clazz.addMethod(proxyMethod);
		return fieldName;
	}

	private static void addDelegatedMethod(CtClass clazz, CtMethod method, CtClass ctDelegate)
			throws NotFoundException, CannotCompileException {
		CtMethod objMethod = ctDelegate.getDeclaredMethod(method.getName(), method.getParameterTypes());
		if (objMethod != null && Modifier.isPublic(objMethod.getModifiers())) {
			CtMethod proxyMethod = CtNewMethod.make(method.getReturnType(), method.getName(),
					method.getParameterTypes(), method.getExceptionTypes(), "{return delegate." + method.getName()
							+ "($$);}", clazz);
			clazz.addMethod(proxyMethod);
		} else {
			throw new NotFoundException("Could not find appropriate public method in object");
		}
	}

	/**
	 * Returns name of field accessed by getter/setter with provided name
	 * according to JavaBeans conventions
	 * 
	 * @param methodName
	 *            name of the invoked getter/setter
	 * @param prefixLength
	 *            length of a prefix (3 for getField and setField methods, 2 for
	 *            isBooleanField method)
	 * @return field name
	 * @throws IndexOutOfBoundsException
	 *             if the provided method name is too short
	 */
	private static final String getBeansFieldName(String methodName, int prefixLength) throws IndexOutOfBoundsException {
		String fieldName = Character.toLowerCase(methodName.charAt(prefixLength))
				+ methodName.substring(prefixLength + 1);
		return fieldName;
	}

	private static final HashMap<String, CtField> processClassFields(CtClass clazz) {
		HashMap<String, CtField> fields = new HashMap<>();
		CtField[] fieldArray = clazz.getDeclaredFields();

		for (CtField field : fieldArray) {
			// Add only those fields that were not defined in subclasses
			fields.putIfAbsent(field.getName(), field);
		}

		return fields;
	}
}
