package ee.ut.jf2014.homework12;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

final public class ProxyInvocationHandler implements InvocationHandler {

	private final Logger log = Logger.getLogger(ProxyInvocationHandler.class);

	private final Object obj;
	private final HashMap<String, List<Method>> methods;
	private final HashMap<String, Field> fields;

	/**
	 * Create new ProxyInvocationHandler.
	 * 
	 * @param obj
	 *            object that will be used by proxy to look up implementations
	 *            of called methods
	 * @throws SecurityException
	 */
	public ProxyInvocationHandler(Object obj) {
		super();
		this.obj = obj;

		Class<?> clazz = obj.getClass();

		// Organize methods and fields in HashMaps for quick access

		methods = new HashMap<>();
		fields = new HashMap<>();

		while (clazz != null) {
			processClassMethods(clazz);
			processClassFields(clazz);

			clazz = clazz.getSuperclass();
		}
	}

	@Override
	public final Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

		log.trace("Processing '" + method.getName() + "'");

		try {
			return invokeDeclaredMethod(method, args);
		} catch (NoSuchMethodException e) {
			log.trace("Could not find declared method");
		} catch (InvocationTargetException e) {
			log.trace("Underlying method threw an exception, propagating it to client code");
			throw e.getCause();
		}

		try {
			return handleBeansGetters(method);
		} catch (NoSuchMethodException e) {
			log.trace("Could not process method as JavaBeans getter");
		}

		try {
			return handleBeansSetters(method, args);
		} catch (NoSuchMethodException e) {
			log.trace("Could not process method as JavaBeans setter");
		}

		log.debug("Could not invoke '" + method.getName() + "'");

		throw new NoSuchMethodException();
	}

	/**
	 * Add all declared public methods of given class to local HashMap.
	 * 
	 * Methods will be added if and only if they are public and it do not have
	 * Deprecated annotation.
	 * 
	 * @param clazz
	 *            class which methods will be added
	 */
	private final void processClassMethods(Class<?> clazz) {
		Method[] methodsArray = clazz.getDeclaredMethods();

		for (Method method : methodsArray) {

			if (!Modifier.isPublic(method.getModifiers())) {
				// Method is not public - ignore
				continue;
			}

			if (method.isAnnotationPresent(Deprecated.class)) {
				// Method is deprecated - ignore
				continue;
			}

			List<Method> cur = methods.get(method.getName());

			if (cur == null) {
				List<Method> newList = new LinkedList<Method>();
				// Note that methods with the same name declared in superclasses
				// will be added after methods of a given class.
				newList.add(method);
				methods.put(method.getName(), newList);
			} else {
				cur.add(method);
			}
		}
	}

	/**
	 * Add all declared fields of a given class to local HashMap if they were
	 * not added before.
	 * 
	 * Note that this method only adds fields that were not defined in
	 * subclasses.
	 * 
	 * @param clazz
	 *            class which fields will be added
	 */
	private final void processClassFields(Class<?> clazz) {
		Field[] fieldArray = clazz.getDeclaredFields();

		for (Field field : fieldArray) {
			// Add only those fields that were not defined in subclasses
			fields.putIfAbsent(field.getName(), field);
		}
	}

	/**
	 * If the provided method adheres to the JavaBean conventions regarding
	 * property setters, and such a field exists, set its value. Throw
	 * NoSuchMethodException otherwise.
	 * 
	 * Note that this method allows access to private and protected fields!
	 * 
	 * @param method
	 *            invoked method
	 * @param args
	 *            an array of objects containing the values of the arguments
	 *            passed in the method invocation on the proxy instance
	 * @return null
	 * @throws NoSuchMethodException
	 *             if the provided method does not adhere to the JavaBeans
	 *             conventions or appropriate field does not exist
	 */
	private final Object handleBeansSetters(Method method, Object[] args) throws NoSuchMethodException {

		if (method.getParameterCount() != 1) {
			log.trace("Wrong parameter count for a setter");
			throw new NoSuchMethodException();
		}

		String methodName = method.getName();
		String fieldName;

		if (!methodName.startsWith("set")) {
			log.trace("Wrong method name for a setter");
			throw new NoSuchMethodException();
		}

		fieldName = getBeansFieldName(methodName, 3);

		Field field = fields.get(fieldName);

		if (field == null) {
			log.trace("Required field not found");
			throw new NoSuchMethodException();
		}

		if (!field.getType().equals(method.getParameterTypes()[0])) {
			log.trace("Field type mismatch");
			throw new NoSuchMethodException();
		}

		try {
			log.trace("Setting field value");
			field.setAccessible(true); // Allow access to private fields
			field.set(obj, args[0]);
			return null;
		} catch (IllegalArgumentException e) {
			// Should not happen
			log.warn(e);
		} catch (IllegalAccessException e) {
			log.trace("Underlying field is not accessible", e);
		}

		// Could not set field
		throw new NoSuchMethodException();
	}

	/**
	 * If the provided method adheres to the JavaBean conventions regarding
	 * property getters, and such a field exists, return its value. Throw
	 * NoSuchMethodException otherwise.
	 * 
	 * Note that this method allows access to private and protected fields!
	 * 
	 * @param method
	 *            invoked method
	 * @return value stored in the field
	 * @throws NoSuchMethodException
	 *             if the provided method does not adhere to the JavaBeans
	 *             conventions or appropriate field does not exist
	 */
	private final Object handleBeansGetters(Method method) throws NoSuchMethodException {

		if (method.getParameterCount() != 0) {
			log.trace("Wrong parameter count for a getter");
			throw new NoSuchMethodException();
		}

		String methodName = method.getName();
		String fieldName;

		if (methodName.startsWith("get") && methodName.length() > 3) {
			fieldName = getBeansFieldName(methodName, 3);
		} else if (methodName.startsWith("is") && methodName.length() > 2
				&& method.getReturnType().equals(boolean.class)) {
			// Boolean getter
			fieldName = getBeansFieldName(methodName, 2);
		} else {
			log.trace("Wrong method name for a getter");
			throw new NoSuchMethodException();
		}

		Field field = fields.get(fieldName);

		if (field == null) {
			log.trace("Required field not found");
			throw new NoSuchMethodException();
		}

		if (!field.getType().equals(method.getReturnType())) {
			log.trace("Field type mismatch");
			throw new NoSuchMethodException();

		}

		log.trace("Returning field value");
		field.setAccessible(true); // Allow access to private fields

		try {
			return field.get(obj);
		} catch (IllegalArgumentException e) {
			// Should not happen
			log.warn(e);
		} catch (IllegalAccessException e) {
			log.trace("Underlying field is not accessible");
		}

		// Could not find needed field
		throw new NoSuchMethodException();
	}

	/**
	 * If a method exists on the object used by this proxy that meets all the
	 * requirements for it to implement the invoked interface method, invoke
	 * that method on obj and return the result, unless the method is marked as
	 * deprecated.
	 * 
	 * @param method
	 *            invoked method
	 * @param args
	 *            an array of objects containing the values of the arguments
	 *            passed in the method invocation on the proxy instance
	 * @return the result of dispatching the method
	 * @throws NoSuchMethodException
	 *             if a method that could implement the requested method was not
	 *             found
	 * @throws InvocationTargetException
	 *             if underlying method threw an exception
	 */
	private final Object invokeDeclaredMethod(Method method, Object[] args) throws NoSuchMethodException,
			InvocationTargetException {

		String methodName = method.getName();

		List<Method> methodsList = methods.get(methodName);

		if (methodsList != null) {
			for (Method objMethod : methodsList) {
				log.trace("Found method " + objMethod.toGenericString());

				if (!objMethod.getReturnType().equals(method.getReturnType())) {
					log.trace("Rejected: return type mismatch");
					continue;
				}

				if (!Arrays.equals(objMethod.getParameterTypes(), method.getParameterTypes())) {
					log.trace("Rejected: parameter type mismatch");
					continue;
				}

				log.trace("Invoking method '" + methodName + "' on object");

				try {
					return objMethod.invoke(obj, args);
				} catch (IllegalAccessException e) {
					log.trace("Underlying method is inaccessible", e);
				} catch (IllegalArgumentException e) {
					// This should not happen
					log.warn(e);
				}
			}
		}

		log.trace("Method not found");
		throw new NoSuchMethodException();
	}

	/**
	 * Returns name of field accessed by getter/setter with provided name
	 * according to JavaBeans conventions
	 * 
	 * @param methodName
	 *            name of the invoked getter/setter
	 * @param prefixLength
	 *            length of a prefix (3 for getField and setField methods, 2 for
	 *            isBooleanField method)
	 * @return field name
	 * @throws IndexOutOfBoundsException
	 *             if the provided method name is too short
	 */
	private final String getBeansFieldName(String methodName, int prefixLength) throws IndexOutOfBoundsException {
		String fieldName = Character.toLowerCase(methodName.charAt(prefixLength))
				+ methodName.substring(prefixLength + 1);
		return fieldName;
	}
}