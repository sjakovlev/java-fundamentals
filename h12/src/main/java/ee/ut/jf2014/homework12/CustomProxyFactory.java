package ee.ut.jf2014.homework12;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtConstructor;
import javassist.CtField;
import javassist.CtMethod;
import javassist.CtNewConstructor;
import javassist.CtNewMethod;
import javassist.Loader;
import javassist.NotFoundException;
import javassist.bytecode.DuplicateMemberException;

public final class CustomProxyFactory {

	private final static String PROXY_CLASS_NAME = "Proxy";

	/**
	 * Return a new dynamic proxy implementing specified interface and using
	 * provided object.
	 * 
	 * Returned proxy will behave in the following way:
	 * 
	 * 1. If a method exists on obj that meets all the requirements for it to
	 * implement the invoked interface method, invoke that method on obj and
	 * return the result, unless the method is marked as deprecated.
	 * 
	 * 2. If the invoked method adhere to the JavaBean conventions regarding
	 * property methods (getters/setters), and such a field exists, access the
	 * field, either getting or setting its value.
	 * 
	 * 3. If no match for (1) or (2) was found, throw a NoSuchMethodException.
	 * 
	 * @param iface
	 *            interface for the proxy class to implement
	 * @param obj
	 *            object on which the methods will be invoked
	 * @return a proxy instance
	 * @throws RuntimeException
	 *             if proxy cannot be created
	 */

	@SuppressWarnings("unchecked")
	public static final <T> T proxify(Class<T> iface, Object obj) {

		ClassPool cp = new ClassPool(true);
		Loader cl = new Loader(obj.getClass().getClassLoader(), cp);

		try {
			// Load previously created class

			Class<?> clazz = cl.loadClass(PROXY_CLASS_NAME + iface.getSimpleName());

			try {
				return (T) clazz.getDeclaredConstructor(obj.getClass()).newInstance(obj);
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
					| InvocationTargetException | NoSuchMethodException | SecurityException e) {
				// Something went wrong during class instantiation.
				throw new RuntimeException("Could not instantiate dynamic proxy", e);
			}

		} catch (ClassNotFoundException e) {
			// Continue
		}

		try {
			Class<?> clazz = proxify_real(iface, obj, cp).toClass();
			return (T) clazz.getDeclaredConstructor(obj.getClass()).newInstance(obj);
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException | NotFoundException | CannotCompileException e) {
			// Something went wrong during class instantiation.
			throw new RuntimeException("Could not create new dynamic proxy", e);
		}
	}

	/**
	 * Returns bytecode for the proxy as defined in documentation of @see
	 * proxify method.
	 * 
	 * @param iface
	 *            interface for the proxy class to implement
	 * @param obj
	 *            object on which the methods will be invoked
	 * @return bytes representind dynamic proxy class
	 * @throws RuntimeException
	 *             if proxy cannot be created
	 */
	public static byte[] proxifyBytes(Class<?> iface, Object obj) {
		ClassPool cp = new ClassPool(true);

		try {
			CtClass clazz = cp.get(PROXY_CLASS_NAME + iface.getSimpleName());
			try {
				return clazz.toBytecode();
			} catch (IOException | CannotCompileException e) {
				throw new RuntimeException(e);
			}
		} catch (NotFoundException e1) {
			// Continue
		}

		try {
			return proxify_real(iface, obj, cp).toBytecode();
		} catch (IllegalArgumentException | CannotCompileException | NotFoundException | IOException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Actual implementation of proxy factory.
	 * 
	 * @see proxify method description for details.
	 * 
	 * @param iface
	 *            interface for the proxy class to implement
	 * @param obj
	 *            object on which the methods will be invoked
	 * @param cp
	 *            ClassPool that will be used to create new class
	 * @return CtClass of newly created dynamic proxy
	 * @throws NotFoundException
	 * @throws CannotCompileException
	 */
	private static final CtClass proxify_real(Class<?> iface, Object obj, ClassPool cp) throws NotFoundException,
			CannotCompileException {

		// Create new empty class

		CtClass clazz;
		clazz = cp.makeClass(PROXY_CLASS_NAME + iface.getSimpleName());

		// Implement given interface

		CtClass ctInterface = cp.get(iface.getName());
		if (!Modifier.isPublic(ctInterface.getModifiers())) {
			throw new CannotCompileException("Proxy only accepts public interfaces");
		}
		clazz.addInterface(ctInterface);

		// Create field for handler

		String handlerFieldName = clazz.makeUniqueName("handler");
		CtClass ctHandler = cp.get(ProxyInvocationHandler.class.getName());
		CtField handler = new CtField(ctHandler, handlerFieldName, clazz);
		handler.setModifiers(Modifier.PRIVATE);
		clazz.addField(handler);

		// Create delegate field

		String delegateFieldName = clazz.makeUniqueName("delegate");
		CtClass ctDelegate = cp.get(obj.getClass().getName());
		CtField delegate = new CtField(ctDelegate, delegateFieldName, clazz);
		delegate.setModifiers(Modifier.PRIVATE);
		clazz.addField(delegate);

		// Create constructor

		CtConstructor constructor = CtNewConstructor.make(new CtClass[] { ctDelegate }, new CtClass[] {}, "{"
				+ delegateFieldName + " = $1; " + handlerFieldName + " = new " + ProxyInvocationHandler.class.getName()
				+ "($1);}", clazz);
		clazz.addConstructor(constructor);

		// Implement all methods via handler

		List<CtMethod> interfaceMethods = getInterfaceMethods(ctInterface);

		for (CtMethod method : interfaceMethods) {

			try {
				CtMethod proxyMethod = CtNewMethod.make(method.getReturnType(), method.getName(),
						method.getParameterTypes(), method.getExceptionTypes(), "{return ($r) " + handlerFieldName
								+ ".invoke($0, this.getClass().getDeclaredMethod(\"" + method.getName()
								+ "\", $sig), $args);}", clazz);
				clazz.addMethod(proxyMethod);
			} catch (DuplicateMemberException e) {
				// Member already implemented, ignore
			}
		}

		return clazz;
	}

	/**
	 * Get all methods declared by given interface and its super-interfaces.
	 * 
	 * @param ctInterface
	 *            an interface
	 * @return list of methods declared by given interface and its super-interfaces
	 */
	private static List<CtMethod> getInterfaceMethods(CtClass ctInterface) {
		List<CtMethod> interfaceMethods = new LinkedList<CtMethod>();

		Deque<CtClass> deque = new ArrayDeque<CtClass>();
		deque.add(ctInterface);

		while (!deque.isEmpty()) {
			CtClass curInterface = deque.poll();

			if (!curInterface.isInterface()) {
				continue;
			}

			CtMethod[] curInterfaceMethods = curInterface.getDeclaredMethods();

			for (CtMethod curInterfaceMethod : curInterfaceMethods) {
				interfaceMethods.add(curInterfaceMethod);
			}

			try {
				for (CtClass superInterface : curInterface.getInterfaces()) {
					deque.add(superInterface);
				}
			} catch (NotFoundException e) {
				// Should not happen
			}
		}
		return interfaceMethods;
	}
}
