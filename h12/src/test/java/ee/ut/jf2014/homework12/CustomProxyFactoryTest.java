package ee.ut.jf2014.homework12;

import static org.junit.Assert.assertEquals;

import java.io.FileNotFoundException;

import javassist.CannotCompileException;

import org.apache.log4j.BasicConfigurator;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class CustomProxyFactoryTest {

	private SimpleTestImpl simpleTestImpl;
	private SimpleTestInterface simpleTestProxy;

	@BeforeClass
	public static final void setUpSuite() {
		BasicConfigurator.configure();
		//Logger.getRootLogger().setLevel(Level.ALL);
	}

	@Before
	public final void setUp() throws InstantiationException, CannotCompileException {
		simpleTestImpl = new SimpleTestImpl();
		simpleTestProxy = CustomProxyFactory.proxify(SimpleTestInterface.class, simpleTestImpl);
	}

	@Test
	public final void testImplementedPublicMethods() {
		simpleTestProxy.setPublicIntWithImplemented(42);
		assertEquals(42, simpleTestProxy.getPublicIntWithImplemented());

		simpleTestProxy.setProtectedIntWithImplemented(42);
		assertEquals(42, simpleTestProxy.getProtectedIntWithImplemented());

		simpleTestProxy.setPrivateIntWithImplemented(42);
		assertEquals(42, simpleTestProxy.getPrivateIntWithImplemented());
	}

	@Test
	public final void testInheritedImplementedPublicMethods() {
		assertEquals("Super test value (int) is 42", simpleTestProxy.superTestRun(42));
		assertEquals("Super test value (String) is TEST", simpleTestProxy.superTestRun("TEST"));
		assertEquals("Super test values are 42, TEST", simpleTestProxy.superTestRun(42, "TEST"));
		assertEquals("Super super test value (int) is 42", simpleTestProxy.superSuperTestRun(42));
	}

	@Test
	public final void testBeans() {
		simpleTestProxy.setPublicIntBeans(42);
		assertEquals(42, simpleTestProxy.getPublicIntBeans());

		simpleTestProxy.setProtectedIntBeans(42);
		assertEquals(42, simpleTestProxy.getProtectedIntBeans());

		simpleTestProxy.setPrivateIntBeans(42);
		assertEquals(42, simpleTestProxy.getPrivateIntBeans());
	}

	@Test
	public final void testSuperBeans() {
		simpleTestProxy.setPublicSuperIntBeans(42);
		assertEquals(42, simpleTestProxy.getPublicSuperIntBeans());

		simpleTestProxy.setProtectedSuperIntBeans(42);
		assertEquals(42, simpleTestProxy.getProtectedSuperIntBeans());

		simpleTestProxy.setPrivateSuperIntBeans(42);
		assertEquals(42, simpleTestProxy.getPrivateSuperIntBeans());
	}

	@Test
	public final void testSuperSuperBeans() {
		simpleTestProxy.setPublicSuperSuperIntBeans(42);
		assertEquals(42, simpleTestProxy.getPublicSuperSuperIntBeans());

		simpleTestProxy.setProtectedSuperSuperIntBeans(42);
		assertEquals(42, simpleTestProxy.getProtectedSuperSuperIntBeans());

		simpleTestProxy.setPrivateSuperSuperIntBeans(42);
		assertEquals(42, simpleTestProxy.getPrivateSuperSuperIntBeans());
	}

	@Test(expected = NoSuchMethodException.class)
	public final void testSetAbsentBeans() throws NoSuchMethodException {
		simpleTestProxy.setAbsentIntBeans(42);
	}

	@Test(expected = NoSuchMethodException.class)
	public final void testSetAbsentBeansWithNoException() {
		simpleTestProxy.setAbsentIntBeansWithNoException(42);
	}

	@Test(expected = NoSuchMethodException.class)
	public final void testGetAbsentBeans() throws NoSuchMethodException {
		simpleTestProxy.getAbsentIntBeans();
	}

	@Test(expected = NoSuchMethodException.class)
	public final void testGetAbsentBeansWithNoException() {
		simpleTestProxy.getAbsentIntBeansWithNoException();
	}

	@Test(expected = NoSuchMethodException.class)
	public final void testAbsentMethod() throws NoSuchMethodException {
		simpleTestProxy.absentMethod();
	}

	@Test(expected = NoSuchMethodException.class)
	public final void testAbsentMethodWithNoException() {
		simpleTestProxy.absentMethodWithNoException();
	}
	
	@Test
	public final void testOverridenMethods() {
		assertEquals("Defined in SimpleTestSuperclassImpl", simpleTestProxy.overridenMethod());
	}
	
	@Test(expected = ArrayIndexOutOfBoundsException.class)
	public final void testUncheckedException() throws ArrayIndexOutOfBoundsException {
		simpleTestProxy.methodWithUncheckedException();
	}
	
	@Test(expected = FileNotFoundException.class)
	public final void testCheckedException() throws FileNotFoundException {
		simpleTestProxy.methodWithCheckedException();
	}

}
