package ee.ut.jf2014.homework12;

public class SimpleTestSuperSuperclassImpl {

	public int publicSuperSuperIntBeans;
	protected int protectedSuperSuperIntBeans;
	@SuppressWarnings("unused")
	private int privateSuperSuperIntBeans;
	
	public SimpleTestSuperSuperclassImpl() {
		super();
	}

	public String superSuperTestRun(int val) {
		return "Super super test value (int) is " + val;
	}
	
	public String overridenMethod() {
		return "Defined in SimpleTestSuperSuperclassImpl";
	}

}