package ee.ut.jf2014.homework12;

import java.io.FileNotFoundException;

public class SimpleTestImpl extends SimpleTestSuperclassImpl {
	
	public int publicInt;
	protected int protectedInt;
	private int privateInt;

	public int publicIntBeans;
	protected int protectedIntBeans;
	@SuppressWarnings("unused")
	private int privateIntBeans;
	
	public int getPublicIntWithImplemented() {
		return publicInt;
	}
	
	public int getProtectedIntWithImplemented() {
		return protectedInt;
	}
	public int getPrivateIntWithImplemented() {
		return privateInt;
	}
	
	public void setPublicIntWithImplemented(int val) {
		publicInt = val;
	}
	
	public void setProtectedIntWithImplemented(int val) {
		protectedInt = val;
	}
	
	public void setPrivateIntWithImplemented(int val) {
		privateInt = val;
	}
	
	public void methodWithUncheckedException() throws ArrayIndexOutOfBoundsException {
		throw new ArrayIndexOutOfBoundsException();
	}
	
	public void methodWithCheckedException() throws FileNotFoundException {
		throw new FileNotFoundException();
	}
	
}
