package ee.ut.jf2014.homework12;

public interface SimpleTestSuperInterface {

	public abstract int getPublicIntWithImplemented();
	public abstract int getProtectedIntWithImplemented();
	public abstract int getPrivateIntWithImplemented();

	public abstract void setPublicIntWithImplemented(int val);
	public abstract void setProtectedIntWithImplemented(int val);
	public abstract void setPrivateIntWithImplemented(int val);

}