package ee.ut.jf2014.homework12;

import java.io.FileNotFoundException;

public interface SimpleTestInterface extends SimpleTestSuperInterface {

	// Duplicate declarations
	public abstract int getPublicIntWithImplemented();
	public abstract int getProtectedIntWithImplemented();
	public abstract int getPrivateIntWithImplemented();
	
	

	public int getPublicIntBeans();
	public int getProtectedIntBeans();
	public int getPrivateIntBeans();
	
	public void setPublicIntBeans(int val);
	public void setProtectedIntBeans(int val);
	public void setPrivateIntBeans(int val);
	
	

	public int getPublicSuperIntBeans();
	public int getProtectedSuperIntBeans();
	public int getPrivateSuperIntBeans();
	
	public void setPublicSuperIntBeans(int val);
	public void setProtectedSuperIntBeans(int val);
	public void setPrivateSuperIntBeans(int val);
	
	
	
	public int getPublicSuperSuperIntBeans();
	public int getProtectedSuperSuperIntBeans();
	public int getPrivateSuperSuperIntBeans();
	
	public void setPublicSuperSuperIntBeans(int val);
	public void setProtectedSuperSuperIntBeans(int val);
	public void setPrivateSuperSuperIntBeans(int val);
	
	
	
	public int getAbsentIntBeans() throws NoSuchMethodException;
	public int setAbsentIntBeans(int val) throws NoSuchMethodException;
	public int getAbsentIntBeansWithNoException();
	public int setAbsentIntBeansWithNoException(int val);
	
	
	
	public String superTestRun(int val);
	public String superTestRun(String val);
	public String superTestRun(int val1, String val2);
	
	
	
	public String superSuperTestRun(int val);
	
	
	
	public int absentMethod() throws NoSuchMethodException;
	public int absentMethodWithNoException();
	
	public void methodWithUncheckedException() throws ArrayIndexOutOfBoundsException;
	public void methodWithCheckedException() throws FileNotFoundException;
	
	public String overridenMethod();
}