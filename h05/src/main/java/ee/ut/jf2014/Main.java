package ee.ut.jf2014;

import java.io.BufferedOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import ee.ut.jf2014.homework5.Account.NegativeSumException;
import ee.ut.jf2014.homework5.Account.NotEnoughMoneyException;
import ee.ut.jf2014.homework5.AccountManager;
import ee.ut.jf2014.homework5.AccountManager.NoSuchAccountException;

public class Main {
	private final static class Donator implements Runnable {
		
		private final static Random rnd = new Random(System.currentTimeMillis());
		private final int id_from;
		private final AccountManager am;
		private final List<Integer> donationOrder;
		private final CountDownLatch latch;

		private Donator(int id, int accountCount, AccountManager am, CountDownLatch latch) {
			super();
			this.id_from = id;
			this.am = am;
			donationOrder = new ArrayList<>(accountCount);
			for (int i = 0; i < accountCount; i++) {
				donationOrder.add(i);
			}
			Collections.shuffle(donationOrder, rnd);
			this.latch = latch;
		}

		@Override
		public void run() {
			int accountTo;
			for (Iterator<Integer> it = donationOrder.iterator(); it
					.hasNext();) {
				accountTo  = (int) it.next();
				try {
					am.transfer(id_from, accountTo, 1);
				} catch (NoSuchAccountException | NegativeSumException
						| NotEnoughMoneyException e) {
					// Could not transfer money. Skip and continue anyway.
					e.printStackTrace();
				}
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					break;
				}
			}
			
			latch.countDown();
		}
		
	}
	
    public static void main(String[] args) {
    	
    	if (args.length != 1) {
    		System.out.println("Usage: program <n>");
    		return;
    	}
    	
    	int n;
    	
    	try {
    		n = Integer.valueOf(args[0]);
    	} catch (NumberFormatException e) {
    		n = -1; // Fail next check and exit.
    	}
    	
    	if (n < 0) {
    		System.out.println("Argument must be a positive integer.");
    		return;
    	}
    	
    	for (int i = 0; i < 1; i++) {
    		performTest(n);
    		System.out.println("===================");
    	}
    }

	private final static void performTest(int n) {
		AccountManager am = new AccountManager(n);
		
		CountDownLatch latch = new CountDownLatch(n);
    	
    	Thread donators[] = new Thread[n];
    	for (int i = 0; i < n; i++) {
    		donators[i] = new Thread(new Donator(i, n, am, latch));
    	}
    	
    	PrintWriter pw = new PrintWriter(new BufferedOutputStream(System.out));
    	
    	am.printBalances(pw);
    	pw.println("=== START ===");
    	
    	for (int i = 0; i < n; i++) {
    		donators[i].start();
    	}
    	
    	while (true) {
    		try {
				if (latch.await(100, TimeUnit.MILLISECONDS)) {
					// All work is done, exit.
					break;
				} else {
					// Still waiting. Print balance and wait some more.
					am.printBalances(pw);
				}
			} catch (InterruptedException e) {
				// Main thread was interrupted. Try to exit normally.
				break;
			}
    	}
    	
    	pw.println("==== END ====");
    	am.printBalances(pw);
    	pw.flush();
	}
}
