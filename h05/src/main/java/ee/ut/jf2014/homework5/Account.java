package ee.ut.jf2014.homework5;

import java.util.concurrent.atomic.AtomicLong;

public final class Account {
	public final static class NegativeSumException extends Exception {
		private static final long serialVersionUID = 1L;
	}

	public final static class NotEnoughMoneyException extends Exception {
		private static final long serialVersionUID = 1L;
	}

	private final int id;
	private final AtomicLong balance;

	public Account(int id, long balance) {
		super();
		this.id = id;
		this.balance = new AtomicLong(balance);
	}

	public final int getId() {
		return id;
	}

	public final long getBalance() {
		return balance.get();
	}

	public final void addToBalance(long sum) throws NegativeSumException {
		if (sum < 0) {
			throw new NegativeSumException();
		}
		balance.addAndGet(sum);
	}

	public final void withdrawFromBalance(long sum)
			throws NegativeSumException, NotEnoughMoneyException {
		if (sum < 0) {
			throw new NegativeSumException();
		}
		synchronized (this) {
			if (sum > getBalance()) {
				throw new NotEnoughMoneyException();
			} else {
				balance.addAndGet(-sum);
			}
		}
	}
}
