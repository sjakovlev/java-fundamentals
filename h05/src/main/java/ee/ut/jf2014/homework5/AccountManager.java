package ee.ut.jf2014.homework5;

import java.io.PrintWriter;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import ee.ut.jf2014.homework5.Account.NegativeSumException;
import ee.ut.jf2014.homework5.Account.NotEnoughMoneyException;

public class AccountManager {
	public final static class NoSuchAccountException extends Exception {
		private static final long serialVersionUID = 1L;
	}

	private final Account[] accounts;
	private final int n;
	private final ReentrantReadWriteLock introspectionLock;

	public AccountManager(int n) throws NegativeArraySizeException {
		super();
		if (n < 0) {
			throw new NegativeArraySizeException(
					"Cannot create negative number of accounts.");
		}
		
		introspectionLock = new ReentrantReadWriteLock();
		
		this.n = n;
		accounts = new Account[n];
		for (int i = 0; i < n; i++) {
			accounts[i] = new Account(i, n);
		}
	}

	public final void transfer(int from, int to, int sum)
			throws NoSuchAccountException, NegativeSumException,
			NotEnoughMoneyException {
		
		if (from < 0 || from >= n || to < 0 || to >= n) {
			throw new NoSuchAccountException();
		}

		if (sum < 0) {
			throw new NegativeSumException();
		}

		if (from == to) {
			return;
		}

		introspectionLock.readLock().lock();
		try {
			accounts[from].withdrawFromBalance(sum);
			accounts[to].addToBalance(sum);
		} finally {
			introspectionLock.readLock().unlock();
		}
	}

	public synchronized final void printBalances(PrintWriter pw) {
		long sum = 0;
		long val;
		introspectionLock.writeLock().lock();
		for (int i = 0; i < n; i++) {
			val = accounts[i].getBalance();
			pw.print(val + " ");
			sum += val;
		}
		introspectionLock.writeLock().unlock();
		pw.println(sum);
	}
}
