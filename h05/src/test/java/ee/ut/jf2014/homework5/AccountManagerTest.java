package ee.ut.jf2014.homework5;

import static org.junit.Assert.*;

import java.io.PrintWriter;
import java.io.StringWriter;

import org.junit.Before;
import org.junit.Test;

import ee.ut.jf2014.homework5.Account.NegativeSumException;
import ee.ut.jf2014.homework5.Account.NotEnoughMoneyException;
import ee.ut.jf2014.homework5.AccountManager.NoSuchAccountException;

public class AccountManagerTest {

	private AccountManager accountManager;

	@Before
	public void setUp() throws Exception {
		accountManager = new AccountManager(3);
	}

	@Test
	public final void testTransferCorrect() throws NoSuchAccountException,
			NegativeSumException, NotEnoughMoneyException {
		accountManager.transfer(0, 1, 3);
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		accountManager.printBalances(pw);
		pw.flush();
		assertEquals("0 6 3 9\n", sw.toString());
	}

	@Test
	public final void testTransferBadFromAccount()
			throws NoSuchAccountException, NegativeSumException,
			NotEnoughMoneyException {
		try {
			accountManager.transfer(3, 1, 1);
			fail("No exception thrown in bad from account.");
		} catch (NoSuchAccountException e) {
			
		}
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		accountManager.printBalances(pw);
		pw.flush();
		assertEquals("3 3 3 9\n", sw.toString());
	}

	@Test
	public final void testTransferBadToAccount() throws NoSuchAccountException,
			NegativeSumException, NotEnoughMoneyException {
		try {
			accountManager.transfer(1, 3, 1);
			fail("No exception thrown in bad to account.");
		} catch (NoSuchAccountException e) {
			
		}
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		accountManager.printBalances(pw);
		pw.flush();
		assertEquals("3 3 3 9\n", sw.toString());
	}

	@Test(expected = NegativeSumException.class)
	public final void testTransferNegative() throws NoSuchAccountException,
			NegativeSumException, NotEnoughMoneyException {
		accountManager.transfer(0, 1, -1);
	}

	@Test(expected = NotEnoughMoneyException.class)
	public final void testTransferTooMuch() throws NoSuchAccountException,
			NegativeSumException, NotEnoughMoneyException {
		accountManager.transfer(0, 1, 5);
	}

	@Test
	public final void testTransferZero() throws NoSuchAccountException,
			NegativeSumException, NotEnoughMoneyException {
		accountManager.transfer(0, 1, 0);
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		accountManager.printBalances(pw);
		pw.flush();
		assertEquals("3 3 3 9\n", sw.toString());
	}

}
