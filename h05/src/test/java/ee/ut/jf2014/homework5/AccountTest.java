package ee.ut.jf2014.homework5;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import ee.ut.jf2014.homework5.Account.NegativeSumException;
import ee.ut.jf2014.homework5.Account.NotEnoughMoneyException;

/**
 * Test Account class methods.
 * 
 * Note that trivial getters are not tested.
 * Also, overflow protection is not implemented or tested!
 * 
 * @author Sergei Jakovlev
 *
 */
public class AccountTest {
	
	private Account testAccount;
	
	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@Before
	public void setUp() throws Exception {
		testAccount = new Account(42, 5000L);
	}

	@Test
	public final void testAccount() {
		assertEquals(42, testAccount.getId());
		assertEquals(5000, testAccount.getBalance());
	}

	@Test
	public final void testAddToBalanceCorrect() throws NegativeSumException {
		testAccount.addToBalance(12345);
		assertEquals(17345, testAccount.getBalance());
	}
	
	@Test(expected = NegativeSumException.class)
	public final void testAddToBalanceNegative() throws NegativeSumException {
		testAccount.addToBalance(-1);
	}
	
	@Test
	public final void testAddToBalanceZero() throws NegativeSumException {
		testAccount.addToBalance(0);
		assertEquals(5000, testAccount.getBalance());
	}
	
	@Test
	public final void testWithdrawFromBalanceCorrect() throws NegativeSumException, NotEnoughMoneyException {
		testAccount.withdrawFromBalance(2000);
		assertEquals(3000, testAccount.getBalance());
	}
	
	@Test
	public final void testWithdrawFromBalanceZero() throws NegativeSumException, NotEnoughMoneyException {
		testAccount.withdrawFromBalance(0);
		assertEquals(5000, testAccount.getBalance());
	}
	
	@Test(expected = NegativeSumException.class)
	public final void testWithdrawFromBalanceNegative() throws NegativeSumException, NotEnoughMoneyException {
		testAccount.withdrawFromBalance(-1);
	}

	@Test(expected = NotEnoughMoneyException.class)
	public final void testWithdrawFromBalanceTooMuch() throws NegativeSumException, NotEnoughMoneyException {
		testAccount.withdrawFromBalance(5001);
	}
}
