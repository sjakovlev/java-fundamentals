package ee.ut.jf2014;

import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import ee.ut.jf2014.homework7.ForkHalfMirror;

public class Main {
    public static void main(String[] args) {
    	if (args.length != 1) {
    		System.out.println("Usage: program <path to image>");
    		return;
    	}
    	
    	try {
			ForkHalfMirror.apply(ImageIO.read(new File(args[0])));
		} catch (IOException e) {
			System.out.println("Error: Could not read file");
		}
    }
}
