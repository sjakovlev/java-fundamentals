package ee.ut.jf2014.homework7;

import java.awt.image.BufferedImage;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class ForkHalfMirror extends RecursiveAction {

	private static final long serialVersionUID = -4917698528209341452L;

	/**
	 * Task size threshold.
	 * 
	 * If current task has to process less lines than this value, they will be processed directly.
	 * Otherwise task will be forked.
	 */
	private static final int maxLinesPerThread = 10;

	private final int[] bitmap;
	
	private final int lineFrom;
	private final int lineTo;
	private final int lineCount;
	private final int lineWidth;

	public ForkHalfMirror(int[] bitmap, int lineFrom, int lineTo, int lineWidth) {
		super();
		
		if (lineFrom < 0 || lineTo < 0 || lineTo < lineFrom || lineTo * lineWidth > bitmap.length) {
			throw new IllegalArgumentException("Bad boundaries provided");
		}
		
		this.bitmap = bitmap;
		this.lineFrom = lineFrom;
		this.lineTo = lineTo;
		this.lineCount = lineTo - lineFrom;
		this.lineWidth = lineWidth;
	}

	@Override
	protected final void compute() {
		if (lineCount <= maxLinesPerThread) {
			computeDirectly();
		} else {
			invokeAll(
					new ForkHalfMirror(bitmap, lineFrom, lineTo - (lineCount / 2), lineWidth),
					new ForkHalfMirror(bitmap, lineTo - (lineCount / 2), lineTo, lineWidth));
		}
	}

	private final void computeDirectly() {
		int halfWidth = lineWidth / 2;
		int iLeft, iRight;
		
		for (int line = lineFrom; line < lineTo; line++) {
			
			// For every line in range
			
			for (int i = 0; i < halfWidth; i++) {
				
				// For every pixel in half of this line
				
				iLeft = line * lineWidth + i;
				iRight = ((line + 1) * lineWidth) - 1 - i;
                
				bitmap[iRight] = avgRGB(bitmap[iLeft], bitmap[iRight]);
			}
		}
	}
	
	/**
	 * Apply half mirror filter to an image and show image before and after in JFrames.
	 * 
	 * @param srcImage Image to which the filter will be applied
	 */
	public static final void apply(BufferedImage srcImage) {
		
		// Show input
		
		showInJframe("Before", srcImage);
		
		// Extract dimensions
		
		int w = srcImage.getWidth();
		int h = srcImage.getHeight();
		
		// Extract pixel array
		
		int[] bitmap = srcImage.getRGB(0, 0, w, h, null, 0, w);
		
		// Process
		
        ForkHalfMirror fhf = new ForkHalfMirror(bitmap, 0, h, w);
        ForkJoinPool pool = new ForkJoinPool();
        pool.invoke(fhf);
        
        // Show result
        
        // Note that transformation itself is done in place using pixel array.
        // Separate image is created for JFrame output only.
        
        BufferedImage dstImage = new BufferedImage(w, h, srcImage.getType());
        dstImage.setRGB(0, 0, w, h, bitmap, 0, w);
        
        showInJframe("After", dstImage);
	}
	
	/**
	 * Compute average of two RGB pixels.
	 * 
	 * Note that this method ignores alpha channel (always 0x00 in output).
	 * 
	 * @param pixel1 One pixel
	 * @param pixel2 Another pixel
	 * @return Integer that represents a pixel with color that is per-channel average of given pixels.
	 */
	private static final int avgRGB(int pixel1, int pixel2) {
		
		int pixel12, rv12, gv12, bv12;
        
        rv12 = (((pixel1 & 0x00ff0000) >> 16) + ((pixel2 & 0x00ff0000) >> 16)) / 2;
        gv12 = (((pixel1 & 0x0000ff00) >> 8 ) + ((pixel2 & 0x0000ff00) >> 8 )) / 2;
        bv12 = (((pixel1 & 0x000000ff) >> 0 ) + ((pixel2 & 0x000000ff) >> 0 )) / 2;
        
        pixel12 = rv12 << 16 | gv12 << 8 | bv12 << 0;
        
        return pixel12;
	}
	
	/**
	 * Show given image in JFrame window with given title.
	 * 
	 * Note that application will exit after all windows are closed.
	 * 
	 * @param title Title of created window
	 * @param img Image that will be displayed
	 */
	private static final void showInJframe(String title, BufferedImage img) {
		JFrame frame = new JFrame(title);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		ImageIcon imageIcon = new ImageIcon(img);
		JLabel label = new JLabel(imageIcon);
		frame.getContentPane().add(label);
		frame.pack();
		frame.setVisible(true);
	}
}
