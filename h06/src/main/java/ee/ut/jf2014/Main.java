package ee.ut.jf2014;

import java.net.MalformedURLException;
import java.net.URL;

import ee.ut.jf2014.homework6.CrawlerDispatcher;

public class Main {
    public static void main(String[] args) {
    	
    	if (args.length < 1 || args.length > 2) {
    		System.out.println("Usage: program <base url> [max links = 100]");
    		return;
    	}
    	
    	String baseUrl = args[0];
    	int maxLinks = 100;
    	
    	if (args.length == 2) {
    		try {
    			maxLinks = Integer.valueOf(args[1]);
    		} catch (NumberFormatException e) {
    			maxLinks = 100;
    		}
    	} else {
    		maxLinks = 100;
    	}
    	
    	try {
			CrawlerDispatcher cd = new CrawlerDispatcher(new URL(baseUrl), maxLinks);
			Thread t = new Thread(cd);
			t.start();
			t.join();
		} catch (MalformedURLException e) {
			System.out.println("Provided URL is malformed");
		} catch (InterruptedException e) {}
    }
}
