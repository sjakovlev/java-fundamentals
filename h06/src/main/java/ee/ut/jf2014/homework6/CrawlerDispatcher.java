package ee.ut.jf2014.homework6;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.io.IOUtils;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.html.HtmlParser;
import org.apache.tika.sax.Link;
import org.apache.tika.sax.LinkContentHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import crawlercommons.robots.BaseRobotRules;
import crawlercommons.robots.SimpleRobotRulesParser;

public class CrawlerDispatcher implements Runnable {

	private class CrawlerWorker implements Runnable {

		protected final Logger log;

		public CrawlerWorker() {
			super();
			log = LoggerFactory.getLogger("CrawlerWorker");
		}

		@Override
		public void run() {

			while (!processingQueue.isEmpty()) {

				URL initialTargetUrl = processingQueue.poll();

				if (initialTargetUrl == null || linksChecked.getAndIncrement() >= linksMax) {
					return;
				}
				
				log.info("Processing page {}", initialTargetUrl);

				try {
					List<Link> links = parsePage(initialTargetUrl).getLinks();

					URLConnection tmpConnection;
					String tmpContentType;

					for (Link link : links) {

						// Clear section part of URL

						String relativeTargetUrl = link.getUri();
						int hashIndex = relativeTargetUrl.indexOf("#");

						if (hashIndex != -1) {
							relativeTargetUrl = relativeTargetUrl.substring(0, hashIndex);
						}

						// Make absolute URL

						URL targetUrl = new URL(initialTargetUrl, relativeTargetUrl);

						// Process URL

						log.info("Found link to {}", targetUrl.toString());

						// Add counter if URL is new
						refCount.putIfAbsent(targetUrl.toString(), new AtomicInteger());

						if (targetUrl.getHost().equals(baseUrl.getHost())) {

							log.debug("Host match for {}", targetUrl.toString());

							if (refCount.get(targetUrl.toString()).getAndIncrement() == 0) {

								log.debug("Did not encounter {} before", targetUrl.toString());

								if (rules.isAllowed(targetUrl.toString())) {

									log.debug("Scanning allowed for {}", targetUrl.toString());

									tmpConnection = targetUrl.openConnection();
									tmpContentType = tmpConnection.getContentType();

									if (tmpContentType.contains("text/html")) {

										processLink(targetUrl);
									}
								}
							}

						} else {
							// Increase counter anyway
							refCount.get(targetUrl.toString()).getAndIncrement();
						}

					}
				} catch (IOException e) {
					log.warn("Page not found or inaccessible: {}", initialTargetUrl.toString());
				}
			}
		}

		protected final LinkContentHandler parsePage(URL link) throws IOException {
			HtmlParser parser = new HtmlParser();

			Metadata metadata = new Metadata();
			ParseContext parseContext = new ParseContext();
			LinkContentHandler handler = new LinkContentHandler();

			try (InputStream inputStream = link.openStream()) {
				parser.parse(inputStream, handler, metadata, parseContext);
				return handler;
			} catch (IOException | SAXException | TikaException e) {
				throw new IOException(e);
			}
		}

		protected void processLink(URL link) {
			try {
				processingQueue.put(link);
			} catch (InterruptedException e) {
				log.warn("Could not add page to queue", e);
			}
		}
	}

	private class InitialCrawlerWorker extends CrawlerWorker {


		public InitialCrawlerWorker(URL targetUrl) {
			super();
			try {
				processingQueue.put(targetUrl);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		@Override
		protected void processLink(URL link) {
			super.processLink(link);
			
			log.info("Starting worker for {}", link.toString());
			Thread t = new Thread(new CrawlerWorker());
			threads.add(t);
			t.start();
		}
	}

	private final static Logger log = LoggerFactory.getLogger("CrawlerDispatcher");
	private final static String botName = "JF-BOT";

	private final URL baseUrl;
	private final int linksMax;
	private final AtomicInteger linksChecked;
	private final ConcurrentHashMap<String, AtomicInteger> refCount;
	private final BlockingQueue<URL> processingQueue;

	private BaseRobotRules rules;

	private final ConcurrentLinkedQueue<Thread> threads;

	public CrawlerDispatcher(URL baseUrl, int linksMax) throws MalformedURLException {
		super();
		this.baseUrl = baseUrl;
		this.linksMax = linksMax;
		linksChecked = new AtomicInteger();
		refCount = new ConcurrentHashMap<>();
		processingQueue = new LinkedBlockingQueue<>();
		threads = new ConcurrentLinkedQueue<>();

		// Get rules from robots.txt
		String hostId = baseUrl.getProtocol() + "://" + baseUrl.getHost()
				+ (baseUrl.getPort() > -1 ? ":" + baseUrl.getPort() : "");

		URL host = new URL(hostId);

		SimpleRobotRulesParser robotRulesParser = new SimpleRobotRulesParser();

		try {
			rules = robotRulesParser
					.parseContent(hostId, IOUtils.toByteArray(host.openStream()), "text/plain", botName);
		} catch (IOException e) {

			log.warn("robots.txt parsing failed, assume allowed");
			log.debug("robots.txt parsing error:", e);

			rules = new BaseRobotRules() {

				@Override
				public boolean isAllowed(String url) {
					return true;
				}

				@Override
				public boolean isAllowNone() {
					return false;
				}

				@Override
				public boolean isAllowAll() {
					return true;
				}
			};
		}

	}

	@Override
	public void run() {

		// Process initial page

		refCount.putIfAbsent(baseUrl.toString(), new AtomicInteger(1));

		log.info("Starting initial worker for base url {}", baseUrl.toString());

		Thread thread = new Thread(new InitialCrawlerWorker(baseUrl));
		threads.add(thread);
		thread.start();

		Thread curThread;

		// Wait for all threads to complete

		try {
			while (!threads.isEmpty()) {
				curThread = (Thread) threads.poll();
				if (curThread != null) {
					curThread.join();
				}
			}
		} catch (InterruptedException e) {
			// Gracefully exit
		}

		// Sort results

		LinkedList<Entry<String, AtomicInteger>> sortedRefCounts = new LinkedList<Entry<String, AtomicInteger>>(
				refCount.entrySet());

		Collections.sort(sortedRefCounts, new Comparator<Entry<String, AtomicInteger>>() {
			// This comparator gives reverse natural ordering of values of
			// entries

			@Override
			public int compare(Entry<String, AtomicInteger> o1, Entry<String, AtomicInteger> o2) {
				return -(Integer.compare(o1.getValue().get(), o2.getValue().get()));
			}
		});

		// Output results

		for (Entry<String, AtomicInteger> entry : sortedRefCounts) {
			System.out.println(entry.getValue().toString() + " : " + entry.getKey());
		}
	}
}
