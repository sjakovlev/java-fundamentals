JVM Troubleshooting and Performance Homework
============

This project has an implemention for the N-puzzle ( http://en.wikipedia.org/wiki/Fifteen_puzzle ). The board is a
little bit bigger though, 5x5. The project uses the class org.zt.jf.perf.Main for benchmarking
the implementation. It uses the A* algorithm by default but is bundled with depth first and breadth first approaches
also.

Your task is to
* Provide me with the command line that would start the program and successfully run it.
 * Probably you want some custom flags here. Let see.
* Describe in a sentence or two why the program is as slow as it is
* Make whatever source code changes you see fit so that the program would run faster. Of course it needs to
produce the same results and solve the same problem :)
* Please provide the code changes and also measurements as how much were you able to improve the speed

Send the results to jf@zeroturnaround.com as you are used to.