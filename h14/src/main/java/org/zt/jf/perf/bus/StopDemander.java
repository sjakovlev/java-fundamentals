package org.zt.jf.perf.bus;

public interface StopDemander {

	boolean isDemandingStop();
	
}
