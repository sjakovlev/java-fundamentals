package ee.ut.jf2014.homework11;

import java.lang.reflect.Proxy;

public final class ProxyFactory {
	/**
	 * Return a new dynamic proxy implementing specified interface and using
	 * provided object.
	 * 
	 * Returned proxy will behave in the following way:
	 * 
	 * 1. If a method exists on obj that meets all the requirements for it to
	 * implement the invoked interface method, invoke that method on obj and
	 * return the result, unless the method is marked as deprecated.
	 * 
	 * 2. If the invoked method adhere to the JavaBean conventions regarding
	 * property methods (getters/setters), and such a field exists, access the
	 * field, either getting or setting its value.
	 * 
	 * 3. If no match for (1) or (2) was found, throw a NoSuchMethodException.
	 * 
	 * @param iface
	 *            interface for the proxy class to implement
	 * @param obj
	 *            object on which the methods will be invoked
	 * @return a proxy instance
	 */
	public static final Object proxify(Class<?> iface, Object obj) {
		Class<?>[] interfaces = { iface };

		return Proxy.newProxyInstance(obj.getClass().getClassLoader(), interfaces, new ProxyInvocationHandler(obj));
	}

}
