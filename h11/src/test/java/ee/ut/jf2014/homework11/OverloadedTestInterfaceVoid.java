package ee.ut.jf2014.homework11;

public interface OverloadedTestInterfaceVoid extends OverloadedTestInterface {
	public void testRun();
}
