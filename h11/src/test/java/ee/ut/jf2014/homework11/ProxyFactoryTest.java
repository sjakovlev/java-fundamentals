package ee.ut.jf2014.homework11;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.UndeclaredThrowableException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Categories.ExcludeCategory;

public class ProxyFactoryTest {

	/**
	 * Loader for local precompiled class files Needed to load OverloadedTest,
	 * which is generated from bytecode.
	 */
	private static class CCL extends ClassLoader {

		private final File pluginLocation = new File("src/test/classes");

		@Override
		protected Class<?> findClass(String name) throws ClassNotFoundException {
			Class<?> clazz;

			// Try to load using parent
			try {
				clazz = getParent().loadClass(name);
				return clazz;
			} catch (ClassNotFoundException e) {
			}

			String classFileLocation = name.substring(name.lastIndexOf(".") + 1) + ".class";
			Path path = FileSystems.getDefault().getPath(pluginLocation.getAbsolutePath(), classFileLocation);

			if (!path.toFile().exists()) {
				throw new ClassNotFoundException(name);
			}

			byte[] bytes = null;
			try {
				bytes = Files.readAllBytes(path);
			} catch (IOException e) {
				throw new RuntimeException(e);
			}

			return defineClass(name, bytes, 0, bytes.length);
		}
	}

	private SimpleTestImpl simpleTestImpl;
	private SimpleTestInterface simpleTestProxy;

	@BeforeClass
	public static final void setUpSuite() {
		BasicConfigurator.configure();
	}

	@Before
	public final void setUp() {
		simpleTestImpl = new SimpleTestImpl();
		simpleTestProxy = (SimpleTestInterface) ProxyFactory.proxify(SimpleTestInterface.class, simpleTestImpl);
	}

	@Test
	public final void testImplementedPublicMethods() {
		simpleTestProxy.setPublicIntWithImplemented(42);
		assertEquals(42, simpleTestProxy.getPublicIntWithImplemented());

		simpleTestProxy.setProtectedIntWithImplemented(42);
		assertEquals(42, simpleTestProxy.getProtectedIntWithImplemented());

		simpleTestProxy.setPrivateIntWithImplemented(42);
		assertEquals(42, simpleTestProxy.getPrivateIntWithImplemented());
	}

	@Test
	public final void testInheritedImplementedPublicMethods() {
		assertEquals("Super test value (int) is 42", simpleTestProxy.superTestRun(42));
		assertEquals("Super test value (String) is TEST", simpleTestProxy.superTestRun("TEST"));
		assertEquals("Super test values are 42, TEST", simpleTestProxy.superTestRun(42, "TEST"));
		assertEquals("Super super test value (int) is 42", simpleTestProxy.superSuperTestRun(42));
	}

	@Test
	public final void testBeans() {
		simpleTestProxy.setPublicIntBeans(42);
		assertEquals(42, simpleTestProxy.getPublicIntBeans());

		simpleTestProxy.setProtectedIntBeans(42);
		assertEquals(42, simpleTestProxy.getProtectedIntBeans());

		simpleTestProxy.setPrivateIntBeans(42);
		assertEquals(42, simpleTestProxy.getPrivateIntBeans());
	}

	@Test
	public final void testSuperBeans() {
		simpleTestProxy.setPublicSuperIntBeans(42);
		assertEquals(42, simpleTestProxy.getPublicSuperIntBeans());

		simpleTestProxy.setProtectedSuperIntBeans(42);
		assertEquals(42, simpleTestProxy.getProtectedSuperIntBeans());

		simpleTestProxy.setPrivateSuperIntBeans(42);
		assertEquals(42, simpleTestProxy.getPrivateSuperIntBeans());
	}

	@Test
	public final void testSuperSuperBeans() {
		simpleTestProxy.setPublicSuperSuperIntBeans(42);
		assertEquals(42, simpleTestProxy.getPublicSuperSuperIntBeans());

		simpleTestProxy.setProtectedSuperSuperIntBeans(42);
		assertEquals(42, simpleTestProxy.getProtectedSuperSuperIntBeans());

		simpleTestProxy.setPrivateSuperSuperIntBeans(42);
		assertEquals(42, simpleTestProxy.getPrivateSuperSuperIntBeans());
	}

	@Test(expected = NoSuchMethodException.class)
	public final void testSetAbsentBeans() throws NoSuchMethodException {
		simpleTestProxy.setAbsentIntBeans(42);
	}

	@Test
	public final void testSetAbsentBeansWithNoException() {
		try {
			simpleTestProxy.setAbsentIntBeansWithNoException(42);
		} catch (UndeclaredThrowableException e) {
			if (!e.getCause().getClass().equals(NoSuchMethodException.class)) {
				fail("Wrong underlying exception (NoSuchMethodException expected)");
			} else {
				return;
			}
		}
		fail("Method must throw UndeclaredThrowableException that wraps NoSuchMethodException");
	}

	@Test(expected = NoSuchMethodException.class)
	public final void testGetAbsentBeans() throws NoSuchMethodException {
		simpleTestProxy.getAbsentIntBeans();
	}

	@Test
	public final void testGetAbsentBeansWithNoException() {
		try {
			simpleTestProxy.getAbsentIntBeansWithNoException();
		} catch (UndeclaredThrowableException e) {
			if (!e.getCause().getClass().equals(NoSuchMethodException.class)) {
				fail("Wrong underlying exception (NoSuchMethodException expected)");
			} else {
				return;
			}
		}
		fail("Method must throw UndeclaredThrowableException that wraps NoSuchMethodException");
	}

	@Test(expected = NoSuchMethodException.class)
	public final void testAbsentMethod() throws NoSuchMethodException {
		simpleTestProxy.absentMethod();
	}

	@Test
	public final void testAbsentMethodWithNoException() {
		try {
			simpleTestProxy.absentMethodWithNoException();
		} catch (UndeclaredThrowableException e) {
			if (!e.getCause().getClass().equals(NoSuchMethodException.class)) {
				fail("Wrong underlying exception (NoSuchMethodException expected)");
			} else {
				return;
			}
		}
		fail("Method must throw UndeclaredThrowableException that wraps NoSuchMethodException");
	}
	
	@Test
	public final void testOverridenMethods() {
		assertEquals("Defined in SimpleTestSuperclassImpl", simpleTestProxy.overridenMethod());
	}
	
	@Test(expected = ArrayIndexOutOfBoundsException.class)
	public final void testUncheckedException() throws ArrayIndexOutOfBoundsException {
		simpleTestProxy.methodWithUncheckedException();
	}
	
	@Test(expected = FileNotFoundException.class)
	public final void testCheckedException() throws FileNotFoundException {
		simpleTestProxy.methodWithCheckedException();
	}

	@Test
	public final void testOverloadedVoid() throws Throwable {

		OverloadedTestInterface overloadedTestImpl = (OverloadedTestInterface) Class.forName("OverloadedTest", true,
				new CCL()).newInstance();

		OverloadedTestInterfaceVoid overloadedTestProxy = (OverloadedTestInterfaceVoid) ProxyFactory.proxify(
				OverloadedTestInterfaceVoid.class, overloadedTestImpl);
		overloadedTestProxy.getClass().getMethod("testRun", new Class<?>[] {}).invoke(overloadedTestProxy);
	}

	@Test
	public final void testOverloadedInt() throws Throwable {

		OverloadedTestInterface overloadedTestImpl = (OverloadedTestInterface) Class.forName("OverloadedTest", true,
				new CCL()).newInstance();

		OverloadedTestInterfaceInt overloadedTestProxy = (OverloadedTestInterfaceInt) ProxyFactory.proxify(
				OverloadedTestInterfaceInt.class, overloadedTestImpl);

		int res = (int) overloadedTestProxy.getClass().getMethod("testRun", new Class<?>[] {})
				.invoke(overloadedTestProxy);

		assertEquals(42, res);
	}

	@Test
	public final void testOverloadedString() throws Throwable {

		OverloadedTestInterface overloadedTestImpl = (OverloadedTestInterface) Class.forName("OverloadedTest", true,
				new CCL()).newInstance();

		OverloadedTestInterfaceString overloadedTestProxy = (OverloadedTestInterfaceString) ProxyFactory.proxify(
				OverloadedTestInterfaceString.class, overloadedTestImpl);

		String res = (String) overloadedTestProxy.getClass().getMethod("testRun", new Class<?>[] {})
				.invoke(overloadedTestProxy);

		assertEquals("Overloaded Test String", res);
	}
}
