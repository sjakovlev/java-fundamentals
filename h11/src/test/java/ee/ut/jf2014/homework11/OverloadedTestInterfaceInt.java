package ee.ut.jf2014.homework11;

public interface OverloadedTestInterfaceInt extends OverloadedTestInterface {
	public int testRun();
}
