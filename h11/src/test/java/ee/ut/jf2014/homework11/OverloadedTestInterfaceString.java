package ee.ut.jf2014.homework11;

public interface OverloadedTestInterfaceString extends OverloadedTestInterface {
	public String testRun();
}
