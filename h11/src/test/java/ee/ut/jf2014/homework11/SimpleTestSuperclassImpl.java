package ee.ut.jf2014.homework11;

public class SimpleTestSuperclassImpl extends SimpleTestSuperSuperclassImpl {
	
	public int publicSuperIntBeans;
	protected int protectedSuperIntBeans;
	@SuppressWarnings("unused")
	private int privateSuperIntBeans;

	public SimpleTestSuperclassImpl() {
		super();
	}

	public String superTestRun(int val) {
		return "Super test value (int) is " + val;
	}

	public String superTestRun(String val) {
		return "Super test value (String) is " + val;
	}

	public String superTestRun(int val1, String val2) {
		return "Super test values are " + val1 + ", " + val2;
	}
	
	@Override
	public String overridenMethod() {
		return "Defined in SimpleTestSuperclassImpl";
	}

}