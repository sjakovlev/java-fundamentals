package ee.ut.jf2014.homework9;

import java.lang.ref.SoftReference;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class FibCached {

    static class MyCache {
    	
    	private HashMap<Long, SoftReference<BigInteger>> cache = new HashMap<>();
    	
    	private long putCount;
    	private long getCount;
    	private long hitCount;
    	private long collectedCount;

        public BigInteger get(Long n) {
        	
        	getCount++;
        	
        	SoftReference<BigInteger> valRef = cache.get(n);
        	
        	if (valRef == null) {
        		return null;
        	}
        	
        	BigInteger val = valRef.get();
        	
        	if (val == null) {
        		collectedCount++;
        		cache.remove(n);
        		return null;
			}
      	
        	hitCount++;
        	return val;
        }

        public void put(Long n, BigInteger result) {
        	putCount++;
        	cache.put(n, new SoftReference<BigInteger>(result));
        }

        public long collectedCount() {
        	for (Iterator<Map.Entry<Long, SoftReference<BigInteger>>> it = cache.entrySet().iterator(); it.hasNext();) {
        		if (it.next().getValue().get() == null) {
        			collectedCount++;
        			it.remove();
        		}
        	}
            return collectedCount;
        }

        public long putCount() {
            return putCount;
        }

        public long getCount() {
            return getCount;
        }

        public long hitCount() {
            return hitCount;
        }
    }

    static MyCache cache = new MyCache();
    
    // this uses too much memory
    // static Map<Long, BigInteger> cache = new HashMap<Long, BigInteger>();

    public static BigInteger fib(long n) {
        if (n == 0 || n == 1) {
            return BigInteger.ONE;
        }
        
        BigInteger cached = cache.get(n);
        if (cached != null) {
            return cached;
        }

        cached = fib(n - 1).add(fib(n - 2));
        cache.put(n, cached);
        return cached;
    }

    public void calculate() {
        int i = 0;
        BigInteger result = null;
        for (i = 0; i < 100000; i++) {
            // long start = System.currentTimeMillis();
            result = fib(i);
            // long end = System.currentTimeMillis();
            // System.out.println(i + " " + result + " " + (end - start));
        }
        System.out.println("done");
        
        // report statistics
        System.out.println("putCount=" + cache.putCount()); // number of calls to cache put method
        System.out.println("getCount=" + cache.getCount()); // number of calls to cache get method
        System.out.println("hitCount=" + cache.hitCount()); // number of calls to cache get method that found a value from cache
        System.out.println("missCount=" + (cache.getCount() - cache.hitCount())); // number of calls to cache get method that did not find a value from cache
        System.out.println("hitRatio=" + ((double) cache.hitCount()) / cache.getCount());
        System.out.println("collectedCount=" + cache.collectedCount()); // number of elements that were put to cache and collected by gc
    }

}
