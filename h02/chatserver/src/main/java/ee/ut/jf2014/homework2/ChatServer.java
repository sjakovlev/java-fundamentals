package ee.ut.jf2014.homework2;

import java.io.Closeable;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.jetty.server.Server;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ChatServer implements Runnable, Closeable {
	
	private final Logger log = LoggerFactory.getLogger(getClass());
	
	private final ServerSocket srv;
	private final Thread serverThread;
	private volatile boolean isClosing = false;
	
	private final List<MessageSender> senders = new LinkedList<>();
	private final MessageReceiver messageReceiver;
	private final Server server;

	public ChatServer(int serverPort, int httpPort) throws IOException {
		
		this.srv = new ServerSocket(serverPort);
		
		this.serverThread = new Thread(this, "acceptor");
		this.serverThread.setDaemon(false);
		this.serverThread.start();
		log.info("Server started on port {}!", serverPort);
		
		messageReceiver = new MessageReceiver(this);
		server = new Server(httpPort);
		server.setHandler(messageReceiver);
		try {
			server.start();
			server.join();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public Thread getThread() {
		return serverThread;
	}

	@Override
	public void run() {
		while (!isClosing && !Thread.currentThread().isInterrupted()) {
			log.info("Waiting for new connection");
			try {
				Socket s = srv.accept();
				log.info("Accepted socket {}", s);
				senders.add(new MessageSender(s));
			} catch (IOException e) {
				if (!isClosing) {
					log.warn("error while handling socket", e);
				} else {
					log.trace("error while handling socket during shutdown", e);
				}
				break;
			}
		}
		
		close();
	}
	
	private void closeSenders() {
		for (MessageSender sender : senders) {
			try {
				sender.close();
			} catch (IOException e) {
				log.warn("Error while closing sender", e);
			}
		}
	}
	
	private void closeReceiver() {
		try {
			server.stop();
		} catch (Exception e) {
			log.warn("Error while closing receiver", e);
		}
	}

	@Override
	public void close() {
		closeReceiver();
		closeSenders();
	}
	
	public void sendMessage(String author, String message) {
		for (MessageSender sender : senders) {
			sender.sendMessage(author, message);
		}
	}
}
