package ee.ut.jf2014;

import java.io.IOException;

import ee.ut.jf2014.homework2.ChatServer;

public class Main {

	public static void main(String[] args) {
		ChatServer cs;
		try {
			cs = new ChatServer(8888, 8080);
			try {
				cs.getThread().join();
			} catch (InterruptedException e) {
				cs.close();
				Thread.currentThread().interrupt();
				return;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
