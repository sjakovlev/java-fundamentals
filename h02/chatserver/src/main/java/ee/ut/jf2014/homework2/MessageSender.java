package ee.ut.jf2014.homework2;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

final class MessageSender implements Runnable, Closeable {
	
	private static int workerCounter = 0;
	
	private final Logger log = LoggerFactory.getLogger(getClass());
	
	private final String name;
	
	private final Socket socket;
	private OutputStreamWriter out;
	private InputStreamReader in;
	
	private final Thread messageSenderThread;
	private boolean isRunning;
	
	private final LinkedBlockingQueue<String> messages;
	
	public MessageSender(Socket socket) {
		this.socket = socket;
		messages = new LinkedBlockingQueue<>();
		name = "MessageSender" + (++workerCounter);
		messageSenderThread = new Thread(this, name);
		messageSenderThread.setDaemon(false);
		messageSenderThread.start();
	}
	
	public String getName() {
		return name;
	}

	@Override
	public void close() throws IOException {
		isRunning = false;
		messageSenderThread.interrupt();
		try {
			out.close();
		} catch (IOException e) {
			log.warn("Error closing output stream", e);
		}
		try {
			in.close();
		} catch (IOException e) {
			log.warn("Error closing input stream", e);
		}
		try {
			socket.close();
		} catch (IOException e) {
			log.warn("Error closing socket", e);
		}
	}

	@Override
	public void run() {
		isRunning = true;
		
		try {
			out = new OutputStreamWriter(socket.getOutputStream(), "UTF-8");
			in = new InputStreamReader(socket.getInputStream(), "UTF-8");
			
			BufferedReader bin = new BufferedReader(in);
			
			String welcomeMessage = bin.readLine();
			log.info("Client sent name: {}", welcomeMessage);
			
			String message;
			
			
			while (isRunning) {
				try {
					message = messages.poll(30, TimeUnit.SECONDS);
					if (message != null) {
						log.info("Sending message: " + message);
						try {
							out.write(message + '\n');
							out.flush();
						} catch (IOException e) {
							log.warn("Could not send message", e);
						}
					}
				} catch (InterruptedException e) {
					log.debug("Sender interrupted", e);
					Thread.currentThread().interrupt();
					break;
				}
			}
			close();
		} catch (IOException e) {
			log.error("Could not create streams", e);
		}
		
	}
	
	public void sendMessage(String author, String message) {
		log.info("Adding message to queue: " + author + ": " + message);
		messages.offer(author + ": " + message);
	}
	
}