package ee.ut.jf2014.homework2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

final class MessageReceiver extends AbstractHandler {
	
	private final Logger log = LoggerFactory.getLogger(getClass());
	private final ChatServer server;
	
	public MessageReceiver(ChatServer server) {
		this.server = server;
	}
	
	@Override
	public void handle(String target, Request baseRequest,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		
		response.setContentType("text/html;charset=utf-8");
        response.setStatus(HttpServletResponse.SC_OK);
        baseRequest.setHandled(true);
        
        String author = request.getHeader("author");
		BufferedReader bin = new BufferedReader(new InputStreamReader(request.getInputStream()));
		String message = bin.readLine();
		
		log.info("Received message {} from {}", message, author);
		
		server.sendMessage(author, message);
	}
	
}