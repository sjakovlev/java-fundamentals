package ee.ut.jf2014.homework3;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.DirectoryStream.Filter;
import java.nio.file.AccessDeniedException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.NotDirectoryException;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchEvent.Kind;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DirectoryMirror {

	private final static class DiffFileFilter implements Filter<Path> {

		private final Path srcPath;

		public DiffFileFilter(Path srcPath) {
			super();
			this.srcPath = srcPath;
		}

		@Override
		public boolean accept(Path entry) throws IOException {
			if (Files.isRegularFile(entry)) {
				Path srcFilePath = srcPath.resolve(entry.getFileName());
				if (!(Files.exists(srcFilePath) && Files
						.isRegularFile(srcFilePath))) {
					return true;
				}
			}
			return false;
		}

	}

	private final static class NewerFileFilter implements Filter<Path> {

		private final Path srcPath;

		public NewerFileFilter(Path srcPath) {
			super();
			this.srcPath = srcPath;
		}

		@Override
		public boolean accept(Path entry) throws IOException {
			if (Files.isRegularFile(entry)) {
				Path srcFilePath = srcPath.resolve(entry.getFileName());
				if (Files.exists(srcFilePath)
						&& Files.isRegularFile(srcFilePath)) {
					if (Files.getLastModifiedTime(entry).compareTo(
							Files.getLastModifiedTime(srcFilePath)) > 0) {
						// Entry was modified later.
						return true;
					}
					return false;
				} else {
					// File does not exist in srcPath. Assume the only file is
					// "newer".
					return true;
				}
			}
			return false;
		}

	}

	private final class ChangeMonitor implements Runnable {

		/**
		 * Delay between checks that source folder exists. Measured in units
		 * defined in pollDelayTimeUnit.
		 */
		private final long pollDelay = 3;
		
		/**
		 * Units in which poll delay is defined.
		 */
		private final TimeUnit pollDelayTimeUnit = TimeUnit.SECONDS;

		@Override
		public void run() {
			WatchKey key;

			while (true) {

				try {
					// Use poll instead of take to periodically check source
					// folder state.
					key = watcher.poll(pollDelay, pollDelayTimeUnit);
				} catch (InterruptedException e) {
					log.log(Level.FINE, "Change monitor interrupted", e);
					break;
				}

				// Note that WatchKey does not become invalid if source
				// directory is deleted on Linux.
				if (!Files.isDirectory(source)) {
					// Source directory was deleted or moved, stop the monitor.
					System.out
							.println("Source directory was deleted or moved, stopping monitor");
					break;
				}

				if (key == null) {
					// No events pending. Continue waiting.
					continue;
				}

				List<WatchEvent<?>> watchEvents = key.pollEvents();

				for (WatchEvent<?> watchEvent : watchEvents) {
					WatchEvent.Kind<?> kind = watchEvent.kind();

					if (kind == StandardWatchEventKinds.OVERFLOW) {
						// Do not process owerflow
						continue;
					}

					@SuppressWarnings("unchecked")
					WatchEvent<Path> ev = (WatchEvent<Path>) watchEvent;

					Path eventPath = ev.context();
					Path srcEvPath = source.resolve(eventPath);
					Path destEvPath = destination.resolve(eventPath);

					Kind<Path> eventKind = ev.kind();

					try {

						if (eventKind == StandardWatchEventKinds.ENTRY_CREATE
								&& Files.isRegularFile(srcEvPath)) {

							System.out.println("Copying new file "
									+ eventPath.getFileName().toString());

							// Note that when editing files in GUI editors
							// ENTRY_CREATE events can be generated for
							// modified files. For this reason do not remove
							// REPLACE_EXISTING option.
							Files.copy(srcEvPath, destEvPath,
									StandardCopyOption.REPLACE_EXISTING);

						} else if (eventKind == StandardWatchEventKinds.ENTRY_DELETE
								&& Files.isRegularFile(destEvPath)) {
							// Note that we will not get here if destination
							// folder is missing. We are processing this in else
							// clause.

							System.out.println("Deleting file "
									+ eventPath.getFileName().toString());

							Files.delete(destEvPath);

						} else if (eventKind == StandardWatchEventKinds.ENTRY_MODIFY) {

							System.out.println("Replacing modified file "
									+ eventPath.getFileName().toString());

							Files.copy(srcEvPath, destEvPath,
									StandardCopyOption.REPLACE_EXISTING);

						} else {
							if (!Files.isDirectory(destination)) {
								// We should get here if destination directory
								// was removed before some file was removed from
								// source directory.
								throw new NoSuchFileException(
										destination.toString());
							}
						}

					} catch (AccessDeniedException e) {
						System.out
								.println("Destination directory unwritable, stopping monitor");
						log.log(Level.WARNING,
								"Destination directory unwritable, stopping monitor",
								e);
						return;

					} catch (NoSuchFileException e) {
						System.out
								.println("Destination directory is missing, stopping monitor");
						log.log(Level.WARNING,
								"Destination directory is missing, stopping monitor",
								e);
						return;

					} catch (IOException e) {
						// If exception is not related to access rights,
						// continue trying.
						System.out.println("Could not synchronize file");
						log.log(Level.WARNING, "Could not synchronize file", e);
					}
				}

				boolean valid = key.reset();
				if (!valid) {
					// Directory is inaccessible or WatchService is closed.
					break;
				}
			}
		}
	}

	private final static Logger log = Logger.getLogger("DirectoryMirror");

	private final Path source;
	private final Path destination;

	private final WatchService watcher;

	/**
	 * This class allows to maintain a complete non-recursive copy of a
	 * directory.
	 * 
	 * Note that only regular files are copied and maintained by this
	 * realization. Directories and other nodes are ignored.
	 * 
	 * @param source
	 *            Path to source directory. Source directory must exist and be
	 *            accessible.
	 * @param destination
	 *            Path to destination directory. It will be created if it does
	 *            not exist.
	 * 
	 * @throws NotDirectoryException
	 *             if source does not point to existing directory
	 * @throws IOException
	 *             if destination directory cannot be created or if an I/O error
	 *             occurs
	 */
	public DirectoryMirror(Path source, Path destination) throws IOException {
		super();

		if (Files.isDirectory(source)) {
			this.source = source;
		} else {
			throw new NotDirectoryException(
					"Source path must point to existing and accessible directory");
		}

		if (Files.isDirectory(destination)) {
			this.destination = destination;
		} else if (Files.exists(destination)) {
			throw new NotDirectoryException(
					"Destination path cannot point to file.");
		} else {
			this.destination = destination;
			System.out.println("Creating destination directory");
			Files.createDirectories(destination);
		}

		watcher = FileSystems.getDefault().newWatchService();
		source.register(watcher, StandardWatchEventKinds.ENTRY_CREATE,
				StandardWatchEventKinds.ENTRY_DELETE,
				StandardWatchEventKinds.ENTRY_MODIFY);

		ChangeMonitor monitor = new ChangeMonitor();

		Thread monitorThread = new Thread(monitor);
		monitorThread.start();

		try {
			sync();
		} catch (IOException e) {
			// Initial sync failed.
			// Stop ChangeMonitor.
			monitorThread.interrupt();
			throw e;
		}
	}

	/**
	 * Perform complete synchronization of directories (source-to-destination).
	 * 
	 * Note that only files are synchronized (directories and their content are
	 * ignored).
	 * 
	 * @throws AccessDeniedException
	 *             if source or destination directories are not accessible
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	private final void sync() throws IOException {
		removeOrphansFormDestination();
		copyUpdatedFilesToDestination();
	}

	/**
	 * Remove files that exist in the destination directory, but do not exist in
	 * the source directory.
	 * 
	 * @throws AccessDeniedException
	 *             if destination directory is not accessible
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	private final void removeOrphansFormDestination() throws IOException {
		DiffFileFilter dff = new DiffFileFilter(source);
		DirectoryStream<Path> destDS = Files.newDirectoryStream(destination,
				dff);

		for (Path orphanFile : destDS) {
			try {
				System.out.println("Deleting file "
						+ orphanFile.getFileName().toString());

				Files.delete(orphanFile);

			} catch (AccessDeniedException e) {
				// Propagate AccessDeniedException to stop the monitor
				throw e;
			} catch (IOException e) {
				System.out.println("Could not delete file");
				log.log(Level.WARNING, "Could not delete file", e);
			}
		}
	}

	/**
	 * Copy files from source to destination if file in source is newer.
	 * 
	 * File will be copied if: * file with such name does not exist in
	 * destination; * file with such name exists in destination, but its last
	 * modified value points to an earlier point in time.
	 * 
	 * @throws IOException
	 *             if an IOError occurs.
	 */
	private final void copyUpdatedFilesToDestination() throws IOException {
		NewerFileFilter nff = new NewerFileFilter(destination);
		DirectoryStream<Path> srcDS = Files.newDirectoryStream(source, nff);

		for (Path newerFile : srcDS) {
			try {
				System.out.println("Copying file "
						+ newerFile.getFileName().toString());
				Path destFilePath = destination
						.resolve(newerFile.getFileName());
				Files.copy(newerFile, destFilePath,
						StandardCopyOption.REPLACE_EXISTING);

			} catch (AccessDeniedException e) {
				// Propagate AccessDeniedException to stop the monitor
				throw e;
			} catch (IOException e) {
				System.out.println("Could not copy file");
				log.log(Level.WARNING, "Could not copy file", e);
			}
		}
	}
}
