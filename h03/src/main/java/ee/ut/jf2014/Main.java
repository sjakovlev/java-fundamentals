package ee.ut.jf2014;

import java.io.IOException;
import java.nio.file.AccessDeniedException;
import java.nio.file.Paths;

import ee.ut.jf2014.homework3.DirectoryMirror;

public class Main {
    public static void main(String[] args) {
    	
    	if (args.length != 2) {
    		System.out.println("Usage: program src_path dest_path");
    		return;
    	}
    	
    	try {
			new DirectoryMirror(Paths.get(args[0]), Paths.get(args[1]));
    	} catch (AccessDeniedException e) {
    		System.out.println("Error: source or destination directory cannot be accessed.");
		} catch (IOException e) {
			System.out.println("Error: " + e.getMessage());
		}
    }
}
